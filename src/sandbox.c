#define _GNU_SOURCE

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <stdbool.h>
#include <signal.h>
#include <sched.h>
#include <stdio.h>
#include <time.h>

#include "sandbox.h"

static char childStack[STACK_SIZE];
static pid_t clientPid;

/*
	Usage message
*/
static void usage(const char* argv0){
	fprintf(stdout, "Usage: %s [OPTIONS ...] [--] EXECUTABLE [ARGS ...]\n", argv0);
	fprintf(stdout, " Runs a executable in a sandboxed environment.\n");
	fprintf(stdout, "\n");
	fprintf(stdout, " Options:\n");
	fprintf(stdout, "  --env                          load environment variables\n");
	fprintf(stdout, "  --writedir \"DIR1;DIR2;...\"     allows writing to these directories\n");
	fprintf(stdout, "  --fwdsocks \"SCK1;SCK2;...\"     forwards specific unix sockets\n");
	fprintf(stdout, "  --fwdtcp                       forward TCP traffic\n");
	fprintf(stdout, "  --fwdudp                       forward UDP traffic\n");
	fprintf(stdout, "  --usrgroup \"GRP1;GRP2;...\"     keep these user-groups\n");
	fprintf(stdout, "  --help                         shows this help\n");
	fprintf(stdout, "\n");
	fprintf(stdout, " Environment variables (multiple values separated by semicolon):\n");
	fprintf(stdout, "  " ENVWRITEDIR "                allows writing to these directories\n");
	fprintf(stdout, "  " ENVFWDSOCKS "                forwards specific unix sockets\n");
	fprintf(stdout, "  " ENVUSRGROUP "                keep these user-groups\n");
	fprintf(stdout, "\n");
	fprintf(stdout, " The sandbox uses the following techniques:\n");
	fprintf(stdout, "  - separate pid namespace\n");
	fprintf(stdout, "  - separate mount namespace\n");
	fprintf(stdout, "  - separate ipc namespace\n");
	fprintf(stdout, "  - separate network namespace      (no network allowed at all!)\n");
	fprintf(stdout, "  - chroot with minimal privileges  (nosuid, readonly, ...)\n");
	fprintf(stdout, "  - deny gaining root again         (only if PR_SET_NO_NEW_PRIVS is available)\n");
	fprintf(stdout, "\n");
	fprintf(stdout, " DISCLAIMER: Depending on what you are going to do these security methods\n");
	fprintf(stdout, "  might not be sufficient! For really evil stuff a virtual machine without\n");
	fprintf(stdout, "  network access should be always preferred!\n");
	fprintf(stdout, "\n");
	fprintf(stdout, "Report bugs to webmaster@fds-team.de\n");
}

/*
	init process inside the sandbox
	(RUNS AS ROOT!)
*/
static int initProcess(struct childDataStruct *data){
	uid_t realUser 	= getuid();
	gid_t realGroup = getgid();
	int res         = 1;
	time_t killTime = 0;
	struct timespec curTime;
	bool childsTerminated;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] initProcess(%p)\n", getpid(), data);
	#endif

	/* check root privileges */
	if (!root_checkPrivileges(realUser, realGroup)){
		fprintf(stderr, "Aborting because of incorrect privileges.\n");
		goto out;
	}

	/* check that we're the init process */
	if (getpid() != 1){
		fprintf(stderr, "You kernel does not support PID namespaces, aborting.\n");
		goto out;
	}

	/* setup the chroot mnt and mark it */
	if (!root_setupChrootMnt(data) || !root_markChroot(data, realUser, realGroup)){
		fprintf(stderr, "Unable to setup chroot, aborting.\n");
		goto out;
	}

	/* do the actual chroot */
	if (!root_doChroot(data->cwDir)){
		fprintf(stderr, "Failed to do the chroot, aborting.\n");
		goto out;
	}

	/* start up loopback device */
	if (!root_interfaceUp("lo", NULL)){
		fprintf(stderr, "Unable to bring up loopback device.\n");
		goto out;
	}

	/* bring up an additional fake interface and set a route */
	if (data->forwardTCP.enabled || data->forwardUDP.enabled){
		struct sockaddr_in fakeInterface;

		memset(&fakeInterface, 0, sizeof(fakeInterface));
		fakeInterface.sin_family 		= AF_INET;
		fakeInterface.sin_addr.s_addr 	= htonl(IPV4_TPROXYIP);
		if (!root_interfaceUp("lo:1", (struct sockaddr *)&fakeInterface)){
			fprintf(stderr, "Unable to bring up loopback device.\n");
			goto out;
		}

		if (!root_setDefaultGateway((struct sockaddr *)&fakeInterface)){
			fprintf(stderr, "Unable to set route gateway.\n");
			goto out;
		}

		if (!root_setupTsocksIPtables(data)){
			fprintf(stderr, "Unable to setup iptables.\n");
			goto out;
		}
	}

	/* bind xserver and other sockets */
	if (!root_bindForwardSockets(data)){
		fprintf(stderr, "Unable to bind forward socket, aborting.\n");
		goto out;
	}

	/* close the socket descriptors */
	clear_ChildDataStruct_ForwardSocket(data, false);

	/* Drop rights */
	if (!root_dropRights(NULL, realUser, realGroup)){
		fprintf(stderr, "Unable to drop root rights.\n");
		goto out;
	}

	/* close all file handles above 3 */
	if (!close_fds_above(3)){
		fprintf(stderr, "Failed to close file descriptors.\n");
		goto out;
	}

	/* let the others in! */
	data->shmData->refCount = 2;
	__sync_compiler_memory_barrier();

	/* wait until our refcount reaches zero */
	while (1){
		sleep(1);
		clock_gettime(CLOCK_MONOTONIC, &curTime);

		/* is the child process tree empty? */
		childsTerminated = process_allchilds_terminated();

		/* more processes left */
		__sync_compiler_memory_barrier();
		if (data->shmData->refCount != 1){
			killTime = 0;
			continue;
		}

		/* give other processes 30 sec to terminate */
		if (killTime == 0) killTime = curTime.tv_sec + 30;

		/* processes running, but time remaining */
		if (!childsTerminated && curTime.tv_sec < killTime)
			continue;

		/* try to terminate, if failed: new process spawned and we have to stay */
		if (!__sync_dec_if_one(&data->shmData->refCount)){
			killTime = 0;
			continue;
		}

		/* terminate */
		break;
	}

	/* open the way for some new init processes */
	data->shmData->initPid = 0;

	/* normal termination */
	res = 0;

out:
	clear_ChildDataStruct(data);
	return res;
}

/*
	Signal handler to redirect everything to the child process
	(runs as regular user)
*/
void sigRedirectToChild(int sig){
	if (clientPid) kill(clientPid, sig);
}

/*
	Switches to the namespace
	(RUNS AS ROOT!)
*/
int main(int argc, char *argv[]){
	uid_t realUser	= getuid();
	gid_t realGroup	= getgid();
	int res 		= 1;
	int argi;
	bool loadEnv	= false;

	struct childDataStruct data;
	pid_t sandboxPid;

	/* initialize struct */
	init_ChildDataStruct(&data);

	/* ensure that we got at least the program filename */
	if (argc < 1 || !argv){
		fprintf(stderr, "Missing argument array, terminating.\n");
		goto out;
	}

	/* command line argument parser */
	for (argi = 1; argi < argc; argi++){
		if (argv[argi][0] != '-' || argv[argi][1] != '-'){
			break;

		}else if (strcmp(argv[argi], "--") == 0){
		 	argi++;
			break;

		}else if (strcmp(argv[argi], "--env") == 0){
			loadEnv = true;

		}else if (strcmp(argv[argi], "--writedir") == 0){
			if (argi + 1 >= argc){ fprintf(stderr, "Option %s requires an argument.\n", argv[argi]); goto out; }
			root_parseWriteDir(argv[++argi], &data, realUser, realGroup);

		}else if (strcmp(argv[argi], "--fwdsocks") == 0){
			if (argi + 1 >= argc){ fprintf(stderr, "Option %s requires an argument.\n", argv[argi]); goto out; }
			root_parseForwardSockets(argv[++argi], &data);

		}else if (strcmp(argv[argi], "--fwdtcp") == 0){
			data.forwardTCP.enabled = true;

		}else if (strcmp(argv[argi], "--fwdudp") == 0){
			data.forwardUDP.enabled = true;

		}else if (strcmp(argv[argi], "--usrgroup") == 0){
			if (argi + 1 >= argc){ fprintf(stderr, "Option %s requires an argument.\n", argv[argi]); goto out; }
			root_parseUserGroups(argv[++argi], &data, realUser, realGroup);

		}else if (argi == 1 && strcmp(argv[argi], "--help") == 0){
			usage(argv[0]);
			res = 0;
			goto out;

		}else{
			fprintf(stderr, "Unrecognized command line argument %s, terminating.\n", argv[argi]);
			goto out;
		}
	}

	/* show usage information */
	if (argi >= argc){
		fprintf(stderr, "Missing command line argument for executable. To get more information type: %s --help\n", argv[0]);
		goto out;
	}

	if (loadEnv){
		root_parseWriteDir(NULL, &data, realUser, realGroup);
		root_parseForwardSockets(NULL, &data);
		root_parseUserGroups(NULL, &data, realUser, realGroup);
	}

	if (!data.writeDir)
		fprintf(stderr, "[SANDBOX] WARNING: You will not have write access to anything except temp dirs!\n");

	if (!data.forwardSocket)
		fprintf(stderr, "[SANDBOX] WARNING: No sockets redirected, you will not have Xserver access!\n");

	/* TODO: can we execute the given file? if not, it makes no sense to continue! */

	/* check root privileges */
	if (!root_checkPrivileges(realUser, realGroup)){
		fprintf(stderr, "Aborting because of incorrect privileges.\n");
		goto out;
	}

	/* get shm data */
	data.shmData = root_getShm(&data, realUser, realGroup);
	if (data.shmData == NULL){
		fprintf(stderr, "Aborting because incorrect IPC.\n");
		goto out;
	}

	/* maybe several attempts required */
	while(1){

		/* do not make any assumptions on the content of shmData */
		__sync_compiler_memory_barrier();

		/* no sandbox available, should we create it? */
		if (__sync_bool_compare_and_swap( &data.shmData->initPid, 0, (pid_t)-1 )){
			fprintf(stderr, "[SANDBOX] Creating new sandbox.\n");

			/* reset refcounter */
			data.shmData->refCount = 0;
			__sync_compiler_memory_barrier();

			/* create socket which will be used for the xserver in the chroot */
			if (!root_createForwardSocketsUnix(&data)){
				fprintf(stderr, "Failed to create UNIX forward sockets, aborting.\n");
				goto err_unlock_pid;
			}

			/* switch to the new namespace */
			sandboxPid = clone(	(int (*)(void *))initProcess, childStack + STACK_SIZE,
								CLONE_NEWPID | CLONE_NEWNS | CLONE_NEWIPC | CLONE_NEWNET | SIGCHLD, &data);
			if (sandboxPid == -1){
				perror("Failed to clone() into new namespace");
				goto err_unlock_pid;
			}

			/* refcounter == 0 means not yet started or crashed */
			while (data.shmData->refCount == 0){
				if (process_child_terminated(sandboxPid)){
					fprintf(stderr, "Failed to initialize sandbox init process, terminating.\n");
					goto err_unlock_pid;
				}

				usleep(100);

				/* ensure that refCount is not cached */
				__sync_compiler_memory_barrier();
			}

			/* enable forwarding */
			if (data.forwardTCP.enabled){
				if (!root_createForwardSocketTCP(&data, sandboxPid)){
					fprintf(stderr, "Failed to create TCP forward sockets, aborting.\n");
					goto out_decref;
				}
			}

			if (data.forwardUDP.enabled){
				if (!root_createForwardSocketUDP(&data, sandboxPid)){
					fprintf(stderr, "Failed to create UDP forward sockets, aborting.\n");
					goto out_decref;
				}
			}

			/* announce the PID */
			data.shmData->initPid = sandboxPid;
			__sync_compiler_memory_barrier();

			/* start socket daemon (will terminate as soon as the init process terminates) */
			if (!root_forwardSocketDaemon(&data, realUser, realGroup, sandboxPid)){
				fprintf(stderr, "Unix/TCP socket forward daemon not running properly.\n");
				goto out_decref;
			}

			/* close sockets, but keep name data (required later) */
			clear_ChildDataStruct_ForwardSocket(&data, true);

			fprintf(stderr, "[SANDBOX] Created sandbox with pid %d.\n", sandboxPid);

		/* there exists a sandbox, try to connect */
		}else if (data.shmData->initPid > 0 && __sync_inc_if_nonzero(&data.shmData->refCount)){

			/* initPid could have changed in the meantime */
			__sync_compiler_memory_barrier();
			sandboxPid = data.shmData->initPid;

			fprintf(stderr, "[SANDBOX] Joining existing sandbox with pid %d.\n", sandboxPid);

			/*  we have to ensure that this pid is really the right sandbox, */
			if ( 	sandboxPid <= 0 || \
					process_nonchild_terminated(sandboxPid) || \
					!root_checkIsSandboxedProcess(sandboxPid) ){

				fprintf(stderr, "Sandbox is not valid anymore, destroying!\n");

				/* unlock this and retry with new sandbox */
				__sync_bool_compare_and_swap(&data.shmData->initPid, sandboxPid, 0);
				continue;
			}

			/* NOTE: if its not the right sandbox, we will notice it by taking a look at the chroot marker */

		}else{
			/* wait and retry */
			usleep(100);
			continue;
		}

		/* close all file handles above 3 */
		if (!close_fds_above(3)){
			fprintf(stderr, "Failed to close file descriptors.\n");
			goto out_decref;
		}

		/* we got a parent process */
		if ( 	!root_switchNS(sandboxPid, "pid", 0, NULL) ||
				!root_switchNS(sandboxPid, "mnt", 0, NULL) ||
				!root_switchNS(sandboxPid, "ipc", CLONE_NEWIPC, NULL) ||
				!root_switchNS(sandboxPid, "net", CLONE_NEWNET, NULL) ){
			fprintf(stderr, "Unable to join namespaces!\n");
			goto out_decref;
		}

		if (!root_checkChrootMarker(&data, realUser, realGroup)){
			fprintf(stderr, "Incorrect or no marker file - hacking attempt?\n");
			goto out_decref;
		}

		if (!root_doChroot(data.cwDir)){
			fprintf(stderr, "Unable to chroot.\n");
			goto out_decref;
		}

		/* Drop rights */
		if (!root_dropRights(&data, realUser, realGroup)){
			fprintf(stderr, "Unable to drop root rights.\n");
			goto out_decref;
		}

		clientPid = fork();
		if (clientPid == 0){

			/* ensure that we're not leaking anything */
			clear_ChildDataStruct(&data);

			/* try again to close opened file descriptors in case there is anything remaining */
			if (!close_fds_above(3)){
				fprintf(stderr, "Failed to close file descriptors.\n");
				exit(1);
			}

			/* check that argv array is nullterminated */
			if (argv[argc] != NULL){
				fprintf(stderr, "Argument array is not nullterminated.\n");
				exit(1);
			}

			/* set environment variables */
			setenv("debian_chroot", "SANDBOX", true);
			setenv(ENVWRITEDIR, "", true);
			setenv(ENVFWDSOCKS, "", true);
			setenv(ENVUSRGROUP, "", true);

			/* execute the program */
			execvp(argv[argi], &argv[argi]);

			/* in case something goes wrong */
			perror("Error while execvp()");
			exit(1);

		}else if (clientPid == -1){
			perror("Unable to fork() child process");
			goto out_decref;
		}

		/* setup signal handler to decrement the refcount */
		signal(SIGINT, sigRedirectToChild);
		signal(SIGTERM, sigRedirectToChild);

		/* wait for child */
		res = process_wait_child_terminated(clientPid);
		goto out_decref;
	}

	/* will never be reached */
	fprintf(stderr, "Well, something went totally wrong inside our code? :-(\n");
	goto out;

err_unlock_pid:
	if (data.shmData) data.shmData->initPid = (pid_t)0;
	goto out;

out_decref:
	if (data.shmData) __sync_fetch_and_add(&data.shmData->refCount, -1);
	/*goto out;*/

out:
	clear_ChildDataStruct(&data);
	return res;
}
