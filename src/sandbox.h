#ifndef Sandbox_h_
#define Sandbox_h_

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <ctype.h>

#define CHROOTDIR		"/var/run/pipelight-sandbox-chroot"
#define CHROOTMARK		"/tmp/pipelight-sandbox"
#define SANDBOXSHM		"/pipelight-sandbox-shm"

#define ENVWRITEDIR		"SANDBOXWRITEDIR"
#define ENVFWDSOCKS 	"SANDBOXFWDSOCKS"
#define ENVUSRGROUP		"SANDBOXUSRGROUP"

#define STACK_SIZE		0x100000
#define SOCKET_FWD_SIZE 0x10000

#define IPV4_LOCALHOST	0x7F000001
#define IPV4_SUBNETMSK  0xFFFFFFFF

#define TPROXY_PORT_TCP 1023
#define TPROXY_PORT_UDP 1022
#define IPV4_TPROXYIP   0xC0000001 /* 192.0.0.1, reserved */

#define DNS_PORT_UDP	53
#define DNS_PORT_TCP	53

/* use raw socket to work around missing kernel features */
#define UDP_RAWSOCKET

#define UDP_TIMEOUT		 180
#define UDP_CLEANUP_INTERVAL 10

#define MAX_LISTEN_EVENTS
#define MAX_EPOLL_EVENTS 10

#if !defined(PATH_MAX) || PATH_MAX < 0 || PATH_MAX > 4096
	#undef PATH_MAX
	#define PATH_MAX 4096
#endif

#define MOUNTPOINT_ERROR	(-1)
#define MOUNTPOINT_NONE  	0
#define MOUNTPOINT_EXISTS	1

struct epollEntry;

typedef bool (* epollCallback)(int efd, struct epollEntry *ep, struct timespec *ct, unsigned int events);

struct epollEntry{
	epollCallback				callback;
};

struct shmDataStruct{
	pid_t 						initPid;		/* Pid of the init process in this sandbox */
	unsigned int 				refCount;		/* refCount (number of processes started + 1) */
};

struct writeDirEntry{
	struct writeDirEntry		*next;			/* next entry */
	char 						*uniqueID;
	char						*path;
};

struct forwardSocketEntry{
	struct epollEntry			epoll;			/* only used in the forward daemon */

	struct forwardSocketEntry	*next; 			/* next entry */
	char 						*uniqueID;
	char 						*path;
	int 						socket;

	uid_t						uid;
	gid_t						gid;
	mode_t						mode;
};

struct forwardNATEntry{							/* only used in the forward daemon */
	struct epollEntry			epoll;

	struct forwardNATEntry		*next;
	struct forwardTCPUDPEntry   *root;
	int 						socket;
	time_t						timeout;
	int 						fromLen;
	struct sockaddr_storage		from;
};

struct forwardTCPUDPEntry{
	struct epollEntry			epoll;			/* only used in the forward daemon */
	struct forwardNATEntry		*nattable;		/* only used in the forward daemon */

	bool						enabled;
	int							socket;
#ifdef UDP_RAWSOCKET
	int 						rawSocket;
#endif
};

struct childDataStruct{
	char						*cwDir;			/* current working dir */
	struct shmDataStruct 		*shmData;		/* shared memory structure */
	struct writeDirEntry		*writeDir;		/* list of write directories */
	struct forwardSocketEntry	*forwardSocket;	/* list of redirect sockets */

	struct forwardTCPUDPEntry	forwardTCP;		/* forward TCP traffic */
	struct forwardTCPUDPEntry	forwardUDP;		/* forward UDP traffic */

	gid_t						*userGroup;
	size_t 						numUserGroups;
};

struct stringListEntry{
	struct stringListEntry		*next;
	char 						*str;
};

/*
	Singly linked list macros
*/

/* insert entry at the beginning of the linked list */
#define SLL_INSERT(head, entry) \
	do{ \
		(entry)->next = (head); \
		(head) = (entry); \
	}while(0)

/* append entry at the end of the linked list (requires valid tail pointer) */
#define SLL_APPEND(head, tail, entry) \
	do{ \
		if (tail) (tail)->next = (entry); else (head) = (entry); \
		(tail) = (entry); \
	}while(0)

/* iterate through all elements starting from head */
#define SLL_FOREACH(head, iter) \
	for ((iter) = (head); (iter); (iter) = (iter)->next)

/* deletes all elements from a list, run fn(iter)
   it is allowed to abort, head is updated approriately */
#define SLL_FOREACH_FREE(head, iter, fn) \
	for ((iter) = (head); (iter); (head) = (iter)->next, fn(iter), (iter) = (head))

/* iterates through a list, safe against removal of entries */
#define SLL_FOREACH_SAFE(head, iter, piter) \
	for ((piter) = &(head), (iter) = (head); (iter); (iter) = (iter) ? ((piter) = &(iter)->next, (iter)->next) : *(piter))

/* remove an element */
#define SLL_REMOVE(iter, piter, fn) \
	do{ \
		*(piter) = (iter)->next; \
		fn(iter); \
		(iter) = NULL; \
	}while(0)

/*
	sec_strncpy
*/
static inline bool sec_strncpy(char *dest, const char *source, size_t num){
	for (; num && *source; dest++, source++, num--)
		*dest = *source;

	if (!num)
		return false;

	*dest = 0;
	return true;
}

/*
	sec_snprintf
*/
static inline bool __check_snprintf(int res, size_t num){
	return (res >= 0 && res < (signed)num);
}
#define sec_snprintf(str, num, format, ...) ((signed)(num) > 0 && __check_snprintf(snprintf(str, num, format, ##__VA_ARGS__), num))

/*
	sec_strnadd
*/
static inline bool sec_strnadd(char **pdest, const char *source, size_t *pnum){
	char *dest = *pdest;
	size_t num = *pnum;

	for (; num && *source; dest++, source++, num--)
		*dest = *source;

	if (!num)
		return false;

	*dest = 0;

	/* update pointers */
	*pdest = dest;
	*pnum  = num;
	return true;
}

/*
	sec_strcat
*/
static inline bool sec_strcat(char *dest, const char *source, size_t num){

	for (; num && *dest; dest++)
		num--;

	for (; num && *source; dest++, source++, num--)
		*dest = *source;

	if (!num)
		return false;

	*dest = 0;
	return true;
}


/*
	sec_readlink
*/
static inline bool sec_readlink(const char *path, char *buf, size_t num){
	ssize_t tmp;

	if ((signed)num <= 0)
		return false;

	tmp = readlink(path, buf, num - 1);
	if (tmp <= 0 || tmp > (signed)(num - 1))
		return false;

	/* make it nullterminated */
	buf[tmp] = 0;
	return true;
}

/*
	str_malloc (creates a copy of a string)
*/
static inline char* str_malloc(const char *str){
	size_t buflen;
	char *buf;

	buflen = strlen(str) + 1;
	if (!(buf = malloc(buflen))){
		perror("Failed to malloc() string");
		return NULL;
	}

	if (!sec_strncpy(buf, str, buflen)){
		free(buf);
		return NULL;
	}

	return buf;
}

/*
	getgroups_malloc (returns a pointer to a -1 terminated list of groupIDs or NULL on error)
*/
static inline gid_t* getgroups_malloc(){
	int numGroups, res;
	gid_t *groups;

	numGroups = getgroups(0, NULL);
	if (numGroups < 0)
		return NULL;

	groups = malloc(sizeof(gid_t) * (numGroups + 1));
	if (!groups)
		return NULL;

	res = getgroups(numGroups, groups);
	if (res < 0 || res > numGroups){
		free(groups);
		return NULL;
	}

	groups[res] = -1; /* make it -1 terminated */
	return groups;
}

/*
	getenv_malloc
*/
static inline char* getenv_malloc(const char *name){
	const char *str = getenv(name);
	return str ? str_malloc(str) : NULL;
}

/*
	Increments the value only if it is nonzero
*/
static inline bool __sync_inc_if_nonzero(unsigned int *ptr){
	unsigned int value;

	for (;;){
		value = *ptr;
		if (!value || __sync_bool_compare_and_swap(ptr, value, value+1)) break;
	}

	return (value != 0);
}

/*
	Decrements the value only if it is one
*/
static inline bool __sync_dec_if_one(unsigned int *ptr){
	return __sync_bool_compare_and_swap(ptr, 1, 0);
}

/*
	Compiler memory barrier
*/
#define __sync_compiler_memory_barrier() \
	asm volatile("" ::: "memory")

/*
	Debugging only
*/
static inline void hexdump(const char *label, void *addr, int len){
	unsigned char *buf = (unsigned char*)addr;
	int i, j;

    if (label != NULL)
        printf ("%s:\n", label);

	for (i = 0; i < len; i += 16){
		printf("%08x: ", i);

		/* hex code */
		for (j = 0; j < 16; j++)
			if (i + j < len)
				printf("%02x ", buf[i+j]);
			else
				printf("   ");

		printf(" ");

		/* chars */
		for (j = 0; j < 16; j++)
			if (i + j < len)
				printf("%c", isprint(buf[i+j]) ? buf[i+j] : '.');

		printf("\n");
	}
}

/* forward.c */
extern bool root_createForwardSocketsUnix(struct childDataStruct *data);
extern bool root_createForwardSocketTCP(struct childDataStruct *data, int sandboxPid);
extern bool root_createForwardSocketUDP(struct childDataStruct *data, int sandboxPid);
extern bool root_bindForwardSockets(const struct childDataStruct *data);
extern bool root_forwardSocketDaemon(struct childDataStruct *data, uid_t realUser, gid_t realGroup, int initPid);
extern bool root_interfaceUp(const char *name, const struct sockaddr *addr);
extern bool root_setDefaultGateway(const struct sockaddr *addr);

/* misc.c */
extern bool dir_starts_with_str(const char *str, const char *pre);
extern bool dir_starts_with(const char *str, const char *pre);
extern bool process_child_terminated(int pid);
extern bool process_nonchild_terminated(int pid);
extern bool process_allchilds_terminated();
extern int process_wait_child_terminated(int pid);
extern bool close_fds_above(int minfd);

/* mount.c */
extern int root_isMountpoint(const char *path);
extern bool root_mkdirRecursive(const char *base, const char *path);
extern bool root_duplicateMnt(const struct childDataStruct *data);
extern bool root_setupChrootMnt(const struct childDataStruct *data);

/* parseenv.c */
extern char* root_uniqueFileID(const struct stat *fileInfo);
extern bool root_generateUniquePath(const char *base, const struct childDataStruct *data, uid_t realUser, gid_t realGroup, char *tmpBuffer, size_t tmpLength);
extern struct writeDirEntry* root_alloc_WriteDir(const char *path, uid_t realUser, gid_t realGroup);
extern bool root_parseWriteDir(const char *arg, struct childDataStruct *data, uid_t realUser, gid_t realGroup);
extern struct forwardSocketEntry* root_alloc_forwardSocket(const char *path);
extern bool root_parseForwardSockets(const char *arg, struct childDataStruct *data);
extern bool root_parseUserGroups(const char *arg, struct childDataStruct *data, uid_t realUser, gid_t realGroup);

/* rights.c */
extern bool root_doChroot(const char *cwd);
extern bool root_dropRights(const struct childDataStruct *data, uid_t realUser, gid_t realGroup);
extern bool root_checkPrivileges(uid_t realUser, gid_t realGroup);
extern bool root_switchNS(pid_t pid , const char *ns, int nstype, int *ret_oldfd);
extern bool root_setNS(int nsfd, int nstype);
extern bool root_checkIsSandboxedProcess(pid_t pid);
extern bool root_markChroot(const struct childDataStruct *data, uid_t realUser, gid_t realGroup);
extern bool root_checkChrootMarker(const struct childDataStruct *data, uid_t realUser, gid_t realGroup);

/* shm.c */
extern struct shmDataStruct* root_getShm(const struct childDataStruct *data, uid_t realUser, gid_t realGroup);

/* struct.c */
extern void init_ChildDataStruct(struct childDataStruct *data);
extern void clear_ChildDataStruct_WriteDir(struct childDataStruct *data);
extern void clear_ChildDataStruct_ForwardSocket(struct childDataStruct *data, bool keepStructs);
extern void clear_ChildDataStruct(struct childDataStruct *data);
extern void free_StringList(struct stringListEntry **list);
extern struct stringListEntry* alloc_StringListEntry(const char *str);

/* iptables.c */
extern bool root_setupTsocksIPtables(struct childDataStruct *data);

#endif // Sandbox_h_