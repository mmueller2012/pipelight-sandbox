#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <errno.h>

#include <libiptc/libiptc.h>
#include <linux/netfilter/xt_TPROXY.h>
#include <linux/netfilter/xt_mark.h>
#include <linux/netfilter/xt_socket.h>
/*#include <linux/netfilter/xt_tcpudp.h>*/
/*#include <linux/netfilter_ipv4/ip_tables.h>*/

#include "sandbox.h"

#define SIZE_ipt_entry 			XT_ALIGN(sizeof(struct ipt_entry))
#define SIZE_ipt_entry_match	XT_ALIGN(sizeof(struct ipt_entry_match))
#define SIZE_xt_socket_mtinfo1	XT_ALIGN(sizeof(struct xt_socket_mtinfo1))
#define SIZE_xt_tcp				XT_ALIGN(sizeof(struct xt_tcp))
#define SIZE_xt_udp				XT_ALIGN(sizeof(struct xt_udp))
#define SIZE_xt_entry_target	XT_ALIGN(sizeof(struct xt_entry_target))
#define SIZE_xt_mark_tginfo2	XT_ALIGN(sizeof(struct xt_mark_tginfo2))
#define SIZE_xt_tproxy_target_info_v1 XT_ALIGN(sizeof(struct xt_tproxy_target_info_v1))

bool root_setupTsocksIPtables(struct childDataStruct *data){
	struct iptc_handle *table;
	bool res = false;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_setupTsocksIPtables(%p)\n", getpid(), data);
	#endif

	if (!data->forwardTCP.enabled && !data->forwardUDP.enabled)
		return true;

	/* initialize mangle table */
	table = iptc_init("mangle");
	if (!table){
		fprintf(stderr, "Failed to get table mangle: %s\n", iptc_strerror(errno));
		return false;
	}

	if (!iptc_create_chain("TPROXYFILTER", table)){
		fprintf(stderr, "Failed to create chain TPROXYFILTER: %s\n", iptc_strerror(errno));
		goto out;
	}

	/* iptables -t mangle -A PREROUTING -j TPROXYFILTER */
	{
		char msg[SIZE_ipt_entry + SIZE_xt_entry_target + SIZE_xt_mark_tginfo2];
		struct ipt_entry *entry = (struct ipt_entry *)msg;
		struct xt_entry_target 		*target;
		memset(msg, 0, sizeof(msg));

		target      = (struct xt_entry_target *)entry->elems;
		target->u.target_size 	= offsetof(struct xt_entry_target, data) + SIZE_xt_mark_tginfo2;
		strncpy(target->u.user.name, "TPROXYFILTER", sizeof(target->u.user.name));

		entry->target_offset 	= SIZE_ipt_entry;
		entry->next_offset 		= entry->target_offset + target->u.target_size;
		entry->ip.proto 		= 0;

		if (entry->next_offset != sizeof(msg)) goto out;
		if (!iptc_append_entry("PREROUTING", entry, table)){
			fprintf(stderr, "Failed to append rule: %s\n", iptc_strerror(errno));
			goto out;
		}
	}

	/* iptables -t mangle -A TPROXYFILTER -m tcp [...] -j RETURN */
	{
		char msg[SIZE_ipt_entry + SIZE_ipt_entry_match + SIZE_xt_tcp + SIZE_xt_entry_target + SIZE_xt_mark_tginfo2];
		struct ipt_entry *entry = (struct ipt_entry *)msg;
		struct ipt_entry_match 		*matchProto;
		struct xt_tcp 				*tcpInfo;
		struct xt_entry_target 		*target;
		memset(msg, 0, sizeof(msg));

		matchProto = (struct ipt_entry_match *)entry->elems;
		matchProto->u.match_size = offsetof(struct ipt_entry_match, data) + SIZE_xt_tcp;
		strncpy(matchProto->u.user.name, "tcp", sizeof(matchProto->u.user.name));

		tcpInfo		= (struct xt_tcp *)matchProto->data;
		tcpInfo->spts[0] = 0;
		tcpInfo->spts[1] = 0xFFFF;
		tcpInfo->dpts[0] = DNS_PORT_TCP;
		tcpInfo->dpts[1] = DNS_PORT_TCP;

		target      = (struct xt_entry_target *)(entry->elems + matchProto->u.match_size);
		target->u.target_size 	= offsetof(struct xt_entry_target, data) + SIZE_xt_mark_tginfo2;
		strncpy(target->u.user.name, "RETURN", sizeof(target->u.user.name));

		entry->target_offset 	= SIZE_ipt_entry + matchProto->u.match_size;
		entry->next_offset 		= entry->target_offset + target->u.target_size;
		entry->ip.proto 		= IPPROTO_TCP;

		if (entry->next_offset != sizeof(msg)) goto out;
		if (!iptc_append_entry("TPROXYFILTER", entry, table)){
			fprintf(stderr, "Failed to append rule: %s\n", iptc_strerror(errno));
			goto out;
		}
	}

	/* iptables -t mangle -A TPROXYFILTER -m udp [...] -j RETURN */
	{
		char msg[SIZE_ipt_entry + SIZE_ipt_entry_match + SIZE_xt_udp + SIZE_xt_entry_target + SIZE_xt_mark_tginfo2];
		struct ipt_entry *entry = (struct ipt_entry *)msg;
		struct ipt_entry_match 		*matchProto;
		struct xt_udp 				*udpInfo;
		struct xt_entry_target 		*target;
		memset(msg, 0, sizeof(msg));

		matchProto = (struct ipt_entry_match *)entry->elems;
		matchProto->u.match_size = offsetof(struct ipt_entry_match, data) + SIZE_xt_udp;
		strncpy(matchProto->u.user.name, "udp", sizeof(matchProto->u.user.name));

		udpInfo		= (struct xt_udp *)matchProto->data;
		udpInfo->spts[0] = 0;
		udpInfo->spts[1] = 0xFFFF;
		udpInfo->dpts[0] = DNS_PORT_UDP;
		udpInfo->dpts[1] = DNS_PORT_UDP;

		target      = (struct xt_entry_target *)(entry->elems + matchProto->u.match_size);
		target->u.target_size 	= offsetof(struct xt_entry_target, data) + SIZE_xt_mark_tginfo2;
		strncpy(target->u.user.name, "RETURN", sizeof(target->u.user.name));

		entry->target_offset 	= SIZE_ipt_entry + matchProto->u.match_size;
		entry->next_offset 		= entry->target_offset + target->u.target_size;
		entry->ip.proto 		= IPPROTO_UDP;

		if (entry->next_offset != sizeof(msg)) goto out;
		if (!iptc_append_entry("TPROXYFILTER", entry, table)){
			fprintf(stderr, "Failed to append rule: %s\n", iptc_strerror(errno));
			goto out;
		}
	}

	/* iptables -t mangle -A TPROXYFILTER -m socket -j ACCEPT */
	{
		char msg[SIZE_ipt_entry + SIZE_ipt_entry_match + SIZE_xt_socket_mtinfo1 + SIZE_xt_entry_target + SIZE_xt_mark_tginfo2];
		struct ipt_entry *entry = (struct ipt_entry *)msg;
		struct ipt_entry_match 		*matchSocket;
		struct xt_socket_mtinfo1 	*socketInfo;
		struct xt_entry_target 		*target;
		memset(msg, 0, sizeof(msg));

		matchSocket = (struct ipt_entry_match *)entry->elems;
		matchSocket->u.match_size = offsetof(struct ipt_entry_match, data) + SIZE_xt_socket_mtinfo1;
		matchSocket->u.user.revision = 1;
		strncpy(matchSocket->u.user.name, "socket", sizeof(matchSocket->u.user.name));

		socketInfo  = (struct xt_socket_mtinfo1 *)matchSocket->data;
		socketInfo->flags = 0;

		target      = (struct xt_entry_target *)(entry->elems + matchSocket->u.match_size);
		target->u.target_size 	= offsetof(struct xt_entry_target, data) + SIZE_xt_mark_tginfo2;
		strncpy(target->u.user.name, "ACCEPT", sizeof(target->u.user.name));

		entry->target_offset 	= SIZE_ipt_entry + matchSocket->u.match_size;
		entry->next_offset 		= entry->target_offset + target->u.target_size;
		entry->ip.proto 		= 0;

		if (entry->next_offset != sizeof(msg)) goto out;
		if (!iptc_append_entry("TPROXYFILTER", entry, table)){
			fprintf(stderr, "Failed to append rule: %s\n", iptc_strerror(errno));
			goto out;
		}
	}

	/* iptables -t mangle -A TPROXYFILTER -d 127.0.0.1 -j ACCEPT */
	{
		char msg[SIZE_ipt_entry + SIZE_xt_entry_target + SIZE_xt_mark_tginfo2];
		struct ipt_entry *entry = (struct ipt_entry *)msg;
		struct xt_entry_target 		*target;
		memset(msg, 0, sizeof(msg));

		target 		= (struct xt_entry_target *)entry->elems;
		target->u.target_size   = offsetof(struct xt_entry_target, data) + SIZE_xt_mark_tginfo2;
		strncpy(target->u.user.name, "ACCEPT", sizeof(target->u.user.name));

		entry->target_offset	= SIZE_ipt_entry;
		entry->next_offset		= entry->target_offset + target->u.target_size;
		entry->ip.dst.s_addr 	= htonl(IPV4_LOCALHOST);
		entry->ip.dmsk.s_addr	= htonl(IPV4_SUBNETMSK);
		entry->ip.proto			= 0;

		if (entry->next_offset != sizeof(msg)) goto out;
		if (!iptc_append_entry("TPROXYFILTER", entry, table)){
			fprintf(stderr, "Failed to append rule: %s\n", iptc_strerror(errno));
			goto out;
		}
	}

	/* iptables -t mangle -A TPROXYFILTER -d 192.0.0.1 -j ACCEPT */
	{
		char msg[SIZE_ipt_entry + SIZE_xt_entry_target + SIZE_xt_mark_tginfo2];
		struct ipt_entry *entry = (struct ipt_entry *)msg;
		struct xt_entry_target 		*target;
		memset(msg, 0, sizeof(msg));

		target 		= (struct xt_entry_target *)entry->elems;
		target->u.target_size   = offsetof(struct xt_entry_target, data) + SIZE_xt_mark_tginfo2;
		strncpy(target->u.user.name, "ACCEPT", sizeof(target->u.user.name));

		entry->target_offset	= SIZE_ipt_entry;
		entry->next_offset		= entry->target_offset + target->u.target_size;
		entry->ip.dst.s_addr 	= htonl(IPV4_TPROXYIP);
		entry->ip.dmsk.s_addr	= htonl(IPV4_SUBNETMSK);
		entry->ip.proto			= 0;

		if (entry->next_offset != sizeof(msg)) goto out;
		if (!iptc_append_entry("TPROXYFILTER", entry, table)){
			fprintf(stderr, "Failed to append rule: %s\n", iptc_strerror(errno));
			goto out;
		}
	}

	/* iptables -t mangle -A PREROUTING -p tcp -j TPROXY --tproxy-mark 0x1/0x1 --on-port 50080 */
	if (data->forwardTCP.enabled){
		char msg[SIZE_ipt_entry + SIZE_xt_entry_target + SIZE_xt_tproxy_target_info_v1];
		struct ipt_entry *entry = (struct ipt_entry *)msg;
		struct xt_entry_target 			*target;
		struct xt_tproxy_target_info_v1 *tproxyInfo;
		memset(msg, 0, sizeof(msg));

		target 		= (struct xt_entry_target *)entry->elems;
		target->u.target_size	= offsetof(struct xt_entry_target, data) + SIZE_xt_tproxy_target_info_v1;
		target->u.user.revision = 1;
		strncpy(target->u.user.name, "TPROXY", sizeof(target->u.user.name));

		tproxyInfo 	= (struct xt_tproxy_target_info_v1 *)target->data;
		tproxyInfo->mark_mask	= 1;
		tproxyInfo->mark_value	= 1;
		tproxyInfo->lport		= htons(TPROXY_PORT_TCP);

		entry->target_offset	= SIZE_ipt_entry;
		entry->next_offset		= entry->target_offset + target->u.target_size;
		entry->ip.proto			= IPPROTO_TCP;

		if (entry->next_offset != sizeof(msg)) goto out;
		if (!iptc_append_entry("PREROUTING", entry, table)){
			fprintf(stderr, "Failed to append rule: %s\n", iptc_strerror(errno));
			goto out;
		}
	}

	/* iptables -t mangle -A PREROUTING -p udp -j TPROXY --tproxy-mark 0x1/0x1 --on-port 50080 */
	if (data->forwardUDP.enabled){
		char msg[SIZE_ipt_entry + SIZE_xt_entry_target + SIZE_xt_tproxy_target_info_v1];
		struct ipt_entry *entry = (struct ipt_entry *)msg;
		struct xt_entry_target 			*target;
		struct xt_tproxy_target_info_v1 *tproxyInfo;
		memset(msg, 0, sizeof(msg));

		target 		= (struct xt_entry_target *)entry->elems;
		target->u.target_size	= offsetof(struct xt_entry_target, data) + SIZE_xt_tproxy_target_info_v1;
		target->u.user.revision = 1;
		strncpy(target->u.user.name, "TPROXY", sizeof(target->u.user.name));

		tproxyInfo 	= (struct xt_tproxy_target_info_v1 *)target->data;
		tproxyInfo->mark_mask	= 1;
		tproxyInfo->mark_value	= 1;
		tproxyInfo->lport		= htons(TPROXY_PORT_UDP);

		entry->target_offset	= SIZE_ipt_entry;
		entry->next_offset		= entry->target_offset + target->u.target_size;
		entry->ip.proto			= IPPROTO_UDP;

		if (entry->next_offset != sizeof(msg)) goto out;
		if (!iptc_append_entry("PREROUTING", entry, table)){
			fprintf(stderr, "Failed to append rule: %s\n", iptc_strerror(errno));
			goto out;
		}
	}

	if (!iptc_commit(table)){
		fprintf(stderr, "Could not commit changes: %s\n", iptc_strerror(errno));
		goto out;
	}

	/* everything okay so far */
	res = true;

out:
	iptc_free(table);

	return res;
}

