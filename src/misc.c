#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>

#include "sandbox.h"

/*
	dir_starts_with_str
*/
bool dir_starts_with_str(const char *str, const char *pre){
	size_t lenstr = strlen(str);
	size_t lenpre = strlen(pre);

	if (lenpre == 0)
		return false;

	if (pre[lenpre-1] == '/')
		lenpre--;

	if (lenstr < lenpre)
		return false;

	if (lenpre > 0 && strncmp(str, pre, lenpre))
		return false;

	return (lenstr == lenpre || str[lenpre] == '/');
}

/*
	dir_starts_with
*/
bool dir_starts_with(const char *str, const char *pre){
	char *canonStr 	= canonicalize_file_name(str);
	char *canonPre 	= canonicalize_file_name(pre);
	bool res 		= dir_starts_with_str(canonStr ? canonStr : str, canonPre ? canonPre : pre);
	if (canonStr) free(canonStr);
	if (canonPre) free(canonPre);
	return res;
}

/*
	process_child_terminated
*/
bool process_child_terminated(int pid){
	int res, status;
	res = waitpid(pid, &status, WNOHANG);
	return (res == -1) || ((res == pid) && WIFEXITED(status));
}

/*
	process_nonchild_terminated
*/
bool process_nonchild_terminated(int pid){
	return (kill(pid, 0) != 0);
}

/*
	process_allchilds_terminated
*/
bool process_allchilds_terminated(){
	int res;
	do{
		res = waitpid(-1, NULL, WNOHANG);
	} while(res > 0);
	return (res == -1 && errno == ECHILD);
}

/*
	process_wait_child_terminated
*/
int process_wait_child_terminated(int pid){
	int res, status;
	res = waitpid(pid, &status, 0);
	return ((res == pid) && WIFEXITED(status)) ? WEXITSTATUS(status) : 1;
}

/*
	Closes all file descriptors above/equal to minfd
*/
bool close_fds_above(int minfd){
	char pathBuffer[PATH_MAX];
	char *endp;
	int fd;
	DIR *dir;
	struct dirent *dirEnt;
	bool res;

	if (!sec_snprintf(pathBuffer, PATH_MAX, "/proc/%ld/fd", (long)getpid()))
		return false;

	dir = opendir(pathBuffer);
	if (!dir){
		perror("Failed to open file descriptor list with opendir()");
		return false;
	}

	res = true;

	while ( (dirEnt = readdir(dir)) ){
		fd = strtol(dirEnt->d_name, &endp, 10);

		if (dirEnt->d_name == endp || *endp != 0)
			continue;

		if (fd < minfd || fd > INT_MAX)
			continue;

		if (fd == dirfd(dir))
			continue;

		/* check that we were really able to close them all */
		if (close(fd) != 0)
			res = false;

	}

	closedir(dir);
	return res;
}
