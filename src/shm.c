#define _GNU_SOURCE

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

#include "sandbox.h"

/*
	Returns a memory location with the mapped shm
	(RUNS AS ROOT)
*/
struct shmDataStruct* root_getShm(const struct childDataStruct *data, uid_t realUser, gid_t realGroup){
	struct shmDataStruct* shmData = NULL;
	char shmBuffer[PATH_MAX];
	int shmfd;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_getShm(%p, %d, %d)\n", getpid(), data, realUser, realGroup);
	#endif

	/* NOTE: this code assumes that the newly created shm is initialized with zeros! */

	/* generate shm path */
	if (!root_generateUniquePath(SANDBOXSHM, data, realUser, realGroup, shmBuffer, PATH_MAX)){
		fprintf(stderr, "Failed to generate shm path.\n");
		return NULL;
	}

	/* open shared memory - has FD_CLOEXEC set automatically */
	shmfd = shm_open(shmBuffer, O_RDWR | O_CREAT, 0600);
	if (shmfd == -1){
		perror("Unable to shm_open() IPC channel");
		return NULL;
	}

	/* ensure that the channel has the required size */
	if (ftruncate(shmfd, sizeof(struct shmDataStruct)) != 0){
		perror("Unable to ftruncate() shm IPC channel");
		goto out;
	}

	/* map it! */
	shmData = mmap(NULL, sizeof(struct shmDataStruct), PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shmData == MAP_FAILED){
		perror("Unable to mmap() shm IPC channel");
		shmData = NULL;
	}

	/* close file descriptor, mmap has incremented the refcount */

out:
	close(shmfd);
	return shmData;
}
