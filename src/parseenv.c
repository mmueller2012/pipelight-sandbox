#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <inttypes.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <grp.h>

#include "sandbox.h"

/*
	Generates an unique ID for the given fileInfo structure
	(RUNS AS ROOT)
*/
char* root_uniqueFileID(const struct stat *fileInfo){
	char buffer[32];

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_uniqueFileID(%p)\n", getpid(), fileInfo);
	#endif

	if (!sec_snprintf(buffer, sizeof(buffer), "-%"PRIx64".%"PRIx64, (uint64_t)fileInfo->st_dev, (uint64_t)fileInfo->st_ino))
		return NULL;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] -> '%s'\n", getpid(), buffer);
	#endif

	return str_malloc(buffer);
}

/*
	Generate a unique path based on the information in the data structure
	(RUNS AS ROOT)
*/
bool root_generateUniquePath(const char *base, const struct childDataStruct *data, uid_t realUser, gid_t realGroup, char *tmpBuffer, size_t tmpLength){
	struct writeDirEntry *writeDir;
	struct forwardSocketEntry *forwardSocket;
	char buffer[32];

	#ifdef SANDBOXDEBUG
		char *startBuffer = tmpBuffer;

		fprintf(stderr, "[SANDBOX:%d] root_generateUniquePath('%s', %p, %d, %d, %p, %zu)\n", getpid(), base, data, realUser, realGroup, tmpBuffer, tmpLength);
	#endif

	if (!sec_snprintf(buffer, sizeof(buffer), "-%x.%x", realUser, realGroup))
		return false;

	if (!sec_strnadd(&tmpBuffer, base, &tmpLength))
		return false;

	if (!sec_strnadd(&tmpBuffer, buffer, &tmpLength))
		return false;

	if (!sec_strnadd(&tmpBuffer, "-wrt", &tmpLength))
		return false;

	SLL_FOREACH(data->writeDir, writeDir){
		if (!sec_strnadd(&tmpBuffer, writeDir->uniqueID, &tmpLength))
			return false;
	}

	if (!sec_strnadd(&tmpBuffer, "-fwd", &tmpLength))
		return false;

	SLL_FOREACH(data->forwardSocket, forwardSocket){
		if (!sec_strnadd(&tmpBuffer, forwardSocket->uniqueID, &tmpLength))
			return false;
	}

	if (data->forwardTCP.enabled){
		if (!sec_strnadd(&tmpBuffer, "-tcp", &tmpLength))
			return false;
	}

	if (data->forwardUDP.enabled){
		if (!sec_strnadd(&tmpBuffer, "-udp", &tmpLength))
			return false;
	}

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] -> '%s'\n", getpid(), startBuffer);
	#endif

	return true;
}

/*
	Allocates a writeDirEntry structure for a given path
	(RUNS AS ROOT)
*/
struct writeDirEntry* root_alloc_WriteDir(const char *path, uid_t realUser, gid_t realGroup){
	struct writeDirEntry *writeDir;
	struct stat fileInfo;
	char *canonPath = NULL;
	char *uniqueID 	= NULL;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_alloc_WriteDir('%s', %d, %d)\n", getpid(), path, realUser, realGroup);
	#endif

	/* get the full path */
	canonPath = canonicalize_file_name(path);
	if (!canonPath){
		perror("Calling canonicalize_file_name() on write directory failed");
		return NULL;
	}

	/* NOTE: we have to free the canonPath ptr when not successful */

	if (stat(canonPath, &fileInfo) != 0){
		perror("Failed to stat() write directory");
		goto err;
	}

	/* this should be a directory */
	if (!S_ISDIR(fileInfo.st_mode)){
		fprintf(stderr, ENVWRITEDIR " should only contain directories.\n");
		goto err;
	}

	/*
		although this is technically not really a security enhancement, since filesystem rights
		are still enforced, but to prevent wrong usage we check if the user owns the directory
	*/
	if (fileInfo.st_uid != realUser || fileInfo.st_gid != realGroup){
		fprintf(stderr, "The write directory must be owned by you.\n");
		goto err;
	}

	uniqueID = root_uniqueFileID(&fileInfo);
	if (!uniqueID){
		fprintf(stderr, "Unable to generate unique id for write directory.\n");
		goto err;
	}

	writeDir = malloc(sizeof(struct writeDirEntry));
	if (!writeDir){
		perror("Failed to malloc() writeDirEntry");
		goto err;
	}

	/* return the datastructure */
	writeDir->next 		= NULL;
	writeDir->uniqueID 	= uniqueID;
	writeDir->path 		= canonPath;
	return writeDir;

err:
	if (uniqueID)	free(uniqueID);
	if (canonPath)	free(canonPath);
	return NULL;
}

/*
	Reads the environment variable WRITEDIR and checks if everything is okay
	(RUNS AS ROOT)
*/
bool root_parseWriteDir(const char *arg, struct childDataStruct *data, uid_t realUser, gid_t realGroup){
	char *listOfDirs, *tmpPtr, *token, *tokPtr;
	struct writeDirEntry *writeDir;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_parseWriteDir('%s', %p, %d, %d)\n", getpid(), arg, data, realUser, realGroup);
	#endif

	/* check for environment variable */
	listOfDirs = arg ? str_malloc(arg) : getenv_malloc(ENVWRITEDIR);
	if (!listOfDirs)
		return false;

	/* NOTE: we have to free listOfDirs */

	for (tmpPtr = listOfDirs; (token = strtok_r(tmpPtr, ";", &tokPtr)) != NULL ; tmpPtr = NULL){
		if (token[0] == 0) continue;

		writeDir = root_alloc_WriteDir(token, realUser, realGroup);
		if (!writeDir){
			fprintf(stderr, "Invalid directory '%s'.\n", token);
			continue;
		}

		/* insert at the beginning of the linked list */
		SLL_INSERT(data->writeDir, writeDir);
	}

	/* free the buffer */
	free(listOfDirs);
	return true;
}

/*
	Allocates a forwardSocketEntry structure
	(RUNS AS ROOT)
*/
struct forwardSocketEntry* root_alloc_forwardSocket(const char *path){
	struct forwardSocketEntry *forwardSocket;
	struct stat fileInfo;
	char *copyPath = NULL;
	char *uniqueID = NULL;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_alloc_forwardSocket('%s')\n", getpid(), path);
	#endif

	if (stat(path, &fileInfo) != 0){
		perror("Failed to stat() unix socket");
		return NULL;
	}

	/* this should be a socket */
	if (!S_ISSOCK(fileInfo.st_mode)){
		fprintf(stderr, ENVFWDSOCKS " should only contain unix sockets.\n");
		return NULL;
	}

	copyPath = str_malloc(path);
	if (!copyPath)
		return NULL; /* already prints an error message */

	uniqueID = root_uniqueFileID(&fileInfo);
	if (!uniqueID){
		fprintf(stderr, "Unable to generate unique id for unix socket.\n");
		goto err;
	}

	forwardSocket = malloc(sizeof(struct forwardSocketEntry));
	if (!forwardSocket){
		perror("Failed to malloc() forwardSocketEntry");
		goto err;
	}

	/* return the datastructure */
	forwardSocket->next 	= NULL;
	forwardSocket->uniqueID = uniqueID;
	forwardSocket->path 	= copyPath;
	forwardSocket->socket 	= (-1);

	forwardSocket->uid 		= fileInfo.st_uid;
	forwardSocket->gid 		= fileInfo.st_gid;
	forwardSocket->mode 	= fileInfo.st_mode;
	return forwardSocket;

err:
	if (uniqueID) free(uniqueID);
	if (copyPath) free(copyPath);
	return NULL;
}

/*
	Reads the environment variable FWDSOCKS and checks if everything is okay
	(RUNS AS ROOT)
*/
bool root_parseForwardSockets(const char *arg, struct childDataStruct *data){
	char *listOfSockets, *tmpPtr, *token, *tokPtr;
	struct forwardSocketEntry *forwardSocket;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_parseForwardSockets('%s', %p)\n", getpid(), arg, data);
	#endif

	/* check for environment variable */
	listOfSockets = arg ? str_malloc(arg) : getenv_malloc(ENVFWDSOCKS);
	if (!listOfSockets)
		return false;

	/* NOTE: we have to free listOfSockets */

	for (tmpPtr = listOfSockets; (token = strtok_r(tmpPtr, ";", &tokPtr)) != NULL ; tmpPtr = NULL){
		if (token[0] == 0) continue;

		forwardSocket = root_alloc_forwardSocket(token);
		if (!forwardSocket){
			fprintf(stderr, "Invalid unix socket '%s'.\n", token);
			continue;
		}

		/* insert at the beginning of the linked list */
		SLL_INSERT(data->forwardSocket, forwardSocket);
	}

	/* free the buffer */
	free(listOfSockets);
	return true;
}

/*
	Reads the environment variable USRGROUP
	(RUNS AS ROOT)
*/
bool root_parseUserGroups(const char *arg, struct childDataStruct *data, uid_t realUser, gid_t realGroup){
	char *listOfGroups, *tmpPtr, *token, *tokPtr;
	struct group *grp;
	gid_t groupID, *newGroups, *groupPtr;
	size_t numGroups;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_parseUserGroups('%s', %p, %d, %d)\n", getpid(), arg, data, realUser, realGroup);
	#endif

	/* check for environment variable */
	listOfGroups = arg ? str_malloc(arg) : getenv_malloc(ENVUSRGROUP);
	if (!listOfGroups)
		return false;

	/* get list of groups */
	if (!data->userGroup)
		data->userGroup = getgroups_malloc();

	if (!data->userGroup){
		perror("Failed to getgroups() of current user");
		free(listOfGroups);
		return false;
	}

	newGroups	= data->userGroup;
	numGroups 	= 0;

	for (tmpPtr = listOfGroups; (token = strtok_r(tmpPtr, ";", &tokPtr)) != NULL ; tmpPtr = NULL){
		if (token[0] == 0) continue;

		grp = getgrnam(token);
		if (grp == NULL){
			fprintf(stderr, "Unable to convert group '%s' to gid.\n", token);
			continue;
		}

		groupID = grp->gr_gid;
		if (groupID == realGroup || groupID == 0 || groupID == getegid())
			continue;

		for (groupPtr = newGroups; *groupPtr != -1 ; groupPtr++){
			if (*groupPtr == groupID){

				if (groupPtr != newGroups){
					*groupPtr = *newGroups;
					*newGroups = groupID;
				}

				newGroups++;
				numGroups++;
				break;
			}
		}

	}

	/* make list (-1) terminated and save the number of elements */
	*newGroups = -1;
	data->numUserGroups	= numGroups;

	/* free the buffers */
	free(listOfGroups);
	return true;
}