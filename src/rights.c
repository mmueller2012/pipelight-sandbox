#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sched.h>
#include <grp.h>

#include "sandbox.h"

#ifndef SANDBOXOLDKERNEL
	#define SANDBOXOLDKERNEL 0
#endif


/*
	Chroots into the specified directory and ensures that no write access is possible!
	(RUNS AS ROOT!)
*/
bool root_doChroot(const char *cwd){
	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_doChroot('%s')\n", getpid(), cwd);
	#endif

	/* switch to chroot */
	if (chroot(CHROOTDIR) != 0){
		perror("Failed to chroot to " CHROOTDIR ".");
		return false;
	}

	/* set current dir (required to ensure that the curdir is not outside) */
	if (chdir("/") != 0){
		perror("Failed to chdir to /.");
		return false;
	}

	/* try to switch back to the original wd after chroot */
	if (cwd){
		if (chdir(cwd) != 0)
			perror("Failed to chdir back to original dir.");
	}

	return true;
}

/*
	Drops the rights to the specified realUser:realGroup
	(RUNS AS ROOT!)

	NOTE: data can be a NULL pointer
*/
bool root_dropRights(const struct childDataStruct *data, uid_t realUser, gid_t realGroup){
	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_dropRights(%p, %d, %d)\n", getpid(), data, realUser, realGroup);
	#endif

	/*
		Disable core dumps for this process and therefore prevent this process from being ptraced.
		This prevents other processes from messing around with the internal state of our program
		and the shared memory which is mapped in our process.
		This should be default for SUID binaries, but better be sure.
	*/
	if (prctl(PR_SET_DUMPABLE, 0, 0, 0, 0) < 0){
		fprintf(stderr, "[SANDBOX] WARNING: PR_SET_DUMPABLE could not be set, the sandbox may be less secure.\n");
	}

	/* Remove or set groups */
	if ((data ? setgroups(data->numUserGroups, data->userGroup) : setgroups(0, NULL)) != 0){
		perror("Failed to setgroups()");
		return false;
	}

	/* Try to set exactly the required rights */
	if (setuid(realUser) != 0 || setgid(realGroup) != 0){
		perror("Failed to setuid()/setgid()");
		return false;
	}

	/* Ensure that this really worked! (if not this would be fatal) */
	if (geteuid() != realUser || getegid() != realGroup ){
		fprintf(stderr, "Dropping root rights failed, uid/gid isn't the expected one.\n");
		return false;
	}

	/* Ensure that we don't have write access to the root directory */
	if (access("/", W_OK) == 0){
		fprintf(stderr, "You have write access to the root directory, sandbox not possible!\n");
		return false;
	}

	/*
		prevent any child process from changing the user (i.e. get root rights)
		(only available since kernel 3.5, but still seems to be buggy in 3.8?)
	*/
	if ( SANDBOXOLDKERNEL || prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0) < 0){
		fprintf(stderr, "[SANDBOX] WARNING: PR_SET_NO_NEW_PRIVS could not be set, the sandbox may be less secure.\n");
	}

	return true;
}

/*
	Check privileges
	(RUNS AS ROOT)
*/
bool root_checkPrivileges(uid_t realUser, gid_t realGroup){
	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_checkPrivileges(%d, %d)\n", getpid(), realUser, realGroup);
	#endif

	/* check our privileges */
	if (geteuid() != 0){
		fprintf(stderr, "This program needs to be setuid and owned by root.\n");
		return false;
	}

	if (realUser == 0 || realGroup == 0){
		fprintf(stderr, "It is not allowed to run this program with root rights.\n");
		return false;
	}

	return true;
}

/*
	Switches to a namespace by PID and name of the corresponding namespace
	(RUNS AS ROOT!)
*/
bool root_switchNS(pid_t pid , const char *ns, int nstype, int *ret_oldfd){
	char pathBuffer[PATH_MAX];
	int nsfd, oldfd = -1;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_switchNS(%d, '%s', %d, %p)\n", getpid(), pid, ns, nstype, ret_oldfd);
	#endif

	/* grab the old namespace descriptor if ret_oldfd is set */
	if (ret_oldfd){
		if (!sec_snprintf(pathBuffer, PATH_MAX, "/proc/%ld/ns/%s", (long)getpid(), ns))
			return false;

		if ((oldfd = open(pathBuffer, O_RDONLY | O_CLOEXEC)) == -1){
			perror("Unable to open() namespace file handle");
			return false;
		}
	}

	if (!sec_snprintf(pathBuffer, PATH_MAX, "/proc/%ld/ns/%s", (long)pid, ns))
		goto err;

	if ((nsfd = open(pathBuffer, O_RDONLY | O_CLOEXEC)) == -1){
		perror("Unable to open() namespace file handle");
		goto err;
	}

	if (!root_setNS(nsfd, nstype))
		goto err;

	if (ret_oldfd)
		*ret_oldfd = oldfd;

	return true;

err:
	if (ret_oldfd)
		close(oldfd);

	return false;
}

/*
	Switches to a namespace by name space file descriptor
	(RUNS AS ROOT!)
*/
bool root_setNS(int nsfd, int nstype){
	bool res = false;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_setNS(%d, %d)\n", getpid(), nsfd, nstype);
	#endif

	res = (setns(nsfd, nstype) == 0);
	if (!res){
		perror("Unable to setns()");
	}

	close(nsfd);
	return res;
}

/*
	Checks if a process is really a chroot namespaced sandbox
	(RUNS AS ROOT!)
*/
bool root_checkIsSandboxedProcess(pid_t pid){
	char pathBuffer[PATH_MAX];
	char resBuffer[PATH_MAX];
	char *canonChrootDir;
	bool res;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_checkIsSandboxedProcess(%d)", getpid(), pid);
	#endif

	if (!sec_snprintf(pathBuffer, PATH_MAX, "/proc/%ld/root", (long)pid))
		return false;

	if (!sec_readlink(pathBuffer, resBuffer, PATH_MAX)){
		perror("Unable to readlink() the root directory of process");
		return false;
	}

	/* get the full path */
	canonChrootDir = canonicalize_file_name(CHROOTDIR);
	if (!canonChrootDir){
		perror("Calling canonicalize_file_name() on " CHROOTDIR " failed");
		return false;
	}

	/* NOTE: we have to free canonChrootDir */

	res = (strcmp(resBuffer, canonChrootDir) == 0);
	if (!res){
		fprintf(stderr, "Hacking attempt! Sandbox initPID points to wrong process.\n");
	}

	free(canonChrootDir);
	return res;
}


/*
	Marks the chroot enviroment
	(RUNS AS ROOT)
*/
bool root_markChroot(const struct childDataStruct *data, uid_t realUser, gid_t realGroup){
	char pathBuffer[PATH_MAX];
	int fd, res;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_markChroot(%p, %d, %d)\n", getpid(), data, realUser, realGroup);
	#endif

	/* generate path */
	if (!root_generateUniquePath(CHROOTDIR CHROOTMARK, data, realUser, realGroup, pathBuffer, PATH_MAX)){
		fprintf(stderr, "Failed to generated marker path.\n");
		return false;
	}

	/* create marker file */
	fd = open(pathBuffer, O_RDWR | O_CREAT | O_EXCL | O_CLOEXEC, 0400);
	if (fd == -1){
		perror("Unable to open() chroot marker file");
		return false;
	}

	/* ensure correct user and group */
	res = (fchown(fd, 0, 0) == 0);

	close(fd);
	return res;
}

/*
	Checks the chroot marker file
	(RUNS AS ROOT)
*/
bool root_checkChrootMarker(const struct childDataStruct *data, uid_t realUser, gid_t realGroup){
	char pathBuffer[PATH_MAX];
	struct stat fileInfo;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_checkChrootMarker(%p, %d, %d)\n", getpid(), data, realUser, realGroup);
	#endif

	/* generate path */
	if (!root_generateUniquePath(CHROOTDIR CHROOTMARK, data, realUser, realGroup, pathBuffer, PATH_MAX)){
		fprintf(stderr, "Failed to generated marker path.\n");
		return false;
	}

	/* get stats of the marker file */
	if (stat(pathBuffer, &fileInfo) != 0){
		perror("Failed to stat() chroot marker file");
		return false;
	}

	/* verify stats */
	if ( !S_ISREG(fileInfo.st_mode) || fileInfo.st_uid != 0 || fileInfo.st_gid != 0 || (fileInfo.st_mode & 0777) != 0400 ){
		fprintf(stderr, "Hacking attempt! Marker file has not the correct attributes.\n");
		return false;
	}

	return true;
}