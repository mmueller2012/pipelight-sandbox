#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <unistd.h>

#include "sandbox.h"

/*
	init_ChildDataStruct
*/
void init_ChildDataStruct(struct childDataStruct *data){
	memset(data, 0, sizeof(*data));

	data->cwDir 		= getcwd(NULL, 0);
	data->shmData 		= NULL;
	data->writeDir 		= NULL;
	data->forwardSocket = NULL;

	data->forwardTCP.enabled	= false;
	data->forwardTCP.socket 	= (-1);

	data->forwardUDP.enabled	= false;
	data->forwardUDP.socket 	= (-1);
#ifdef UDP_RAWSOCKET
	data->forwardUDP.rawSocket	= (-1);
#endif

	data->userGroup		= NULL;
	data->numUserGroups = 0;
}

/*
	clear_ChildDataStruct_WriteDir
*/
void clear_ChildDataStruct_WriteDir(struct childDataStruct *data){
	struct writeDirEntry *writeDir;

	SLL_FOREACH_FREE(data->writeDir, writeDir, free){
		if (writeDir->uniqueID)	free(writeDir->uniqueID);
		if (writeDir->path) 	free(writeDir->path);
	}
}

/*
	clear_ChildDataStruct_ForwardSocket
*/
void clear_ChildDataStruct_ForwardSocket(struct childDataStruct *data, bool keepStructs){
	struct forwardSocketEntry *forwardSocket;

	if (keepStructs){
		SLL_FOREACH(data->forwardSocket, forwardSocket){
			if (forwardSocket->socket != (-1)){
				close(forwardSocket->socket);
				forwardSocket->socket = (-1);
			}
		}

	}else{
		SLL_FOREACH_FREE(data->forwardSocket, forwardSocket, free){
			if (forwardSocket->socket != (-1))
				close(forwardSocket->socket);

			if (forwardSocket->uniqueID)	free(forwardSocket->uniqueID);
			if (forwardSocket->path) 		free(forwardSocket->path);
		}
	}

	if (data->forwardTCP.socket != (-1)){
		close(data->forwardTCP.socket);
		data->forwardTCP.socket = (-1);
	}

	if (data->forwardUDP.socket != (-1)){
		close(data->forwardUDP.socket);
		data->forwardUDP.socket = (-1);
	}

#ifdef UDP_RAWSOCKET
	if (data->forwardUDP.rawSocket != (-1)){
		close(data->forwardUDP.rawSocket);
		data->forwardUDP.rawSocket = (-1);
	}
#endif

}

/*
	clear_ChildDataStruct
*/
void clear_ChildDataStruct(struct childDataStruct *data){
	if (data->cwDir)	free(data->cwDir);
	if (data->shmData) 	munmap(data->shmData, sizeof(struct shmDataStruct));
	if (data->userGroup) free(data->userGroup);
	data->cwDir 		= NULL;
	data->shmData 		= NULL;
	data->userGroup 	= NULL;
	clear_ChildDataStruct_WriteDir(data);
	clear_ChildDataStruct_ForwardSocket(data, false);
}

/*
	free_StringList
*/
void free_StringList(struct stringListEntry **list){
	struct stringListEntry *entry;

	SLL_FOREACH_FREE(*list, entry, free){
		if (entry->str) free(entry->str);
	}
}

/*
	alloc_StringListEntry
*/
struct stringListEntry* alloc_StringListEntry(const char *str){
	struct stringListEntry *entry;
	char *copyStr;

	/* get the full path */
	copyStr = str_malloc(str);
	if (!copyStr)
		return NULL; /* already prints an error message */

	entry = malloc(sizeof(struct stringListEntry));
	if (!entry){
		perror("Failed to malloc() stringListEntry");
		free(copyStr);
		return NULL;
	}

	/* return the datastructure */
	entry->next 		= NULL;
	entry->str 			= copyStr;
	return entry;
}