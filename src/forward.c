#define _GNU_SOURCE

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/epoll.h>
#include <net/route.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <string.h>
#include <sys/un.h>
#include <signal.h>
#include <net/if.h>
#include <stdio.h>
#include <netdb.h>
#include <sched.h>
#include <time.h>

#include "sandbox.h"

#ifdef UDP_RAWSOCKET

#include <netinet/udp.h>
#include <netinet/ip.h>

/* calculates the checksum for an IP header */
uint16_t in_cksum(void *ip, size_t len){
	uint8_t *p 		= ip;
	uint32_t sum 	= 0;

	while (len > 1){
		sum += *p++ << 8;
		sum += *p++;
		len -= 2;
	}

	if (len)
		sum += *p << 8;

	sum  = (sum >> 16) + (sum & 0xFFFF);
	sum += sum >> 16;
	sum  = ntohs(sum);
	return (0xFFFF & ~sum);
}

#endif


/*
	CMSG_DATA
*/
#define CMSG_DATALEN(cmsg) \
	( cmsg->cmsg_len - sizeof(struct cmsghdr) )

/*
	Sockaddr operations
*/

/* determines the length of the sockaddr_storage structure based on the protocol and maximum length */
static inline size_t sockaddr_length(struct sockaddr_storage *a, size_t len){
	if (len < offsetof(struct sockaddr_storage, ss_family) + sizeof(a->ss_family)) goto err;

	if (a->ss_family == AF_INET){
		if (len < sizeof(struct sockaddr_in)) goto err;
		return sizeof(struct sockaddr_in);
	}

	if (a->ss_family == AF_INET6){
		if (len < sizeof(struct sockaddr_in6)) goto err;
		return sizeof(struct sockaddr_in6);
	}

err:
	return 0;
}

/* compares two sockaddr_storage structures */
static inline bool compare_sockaddr(struct sockaddr_storage *a, struct sockaddr_storage *b){
	if (a->ss_family != b->ss_family)
		return false;

	if (a->ss_family == AF_INET)
		return (	((struct sockaddr_in *)a)->sin_port == ((struct sockaddr_in *)b)->sin_port &&
					((struct sockaddr_in *)a)->sin_addr.s_addr == ((struct sockaddr_in *)b)->sin_addr.s_addr );

	if (a->ss_family == AF_INET6)
		return (	((struct sockaddr_in6 *)a)->sin6_port == ((struct sockaddr_in6 *)b)->sin6_port &&
					memcmp( &((struct sockaddr_in6 *)a)->sin6_addr, &((struct sockaddr_in6 *)b)->sin6_addr, sizeof(struct in6_addr) ) == 0 );

	/* unsupported sockaddr type */
	return false;
}

static char* sockaddr_to_str(struct sockaddr_storage *a){
	static char buffer[1024];

	if (a->ss_family == AF_INET){
		if (!sec_snprintf(buffer, sizeof(buffer), "sockaddr_in( family=AF_INET, %s:%d )",
				inet_ntoa(((struct sockaddr_in *)a)->sin_addr), ntohs(((struct sockaddr_in *)a)->sin_port) ))
			goto invalid;

		return buffer;
	}

invalid:
	if (!sec_snprintf(buffer, sizeof(buffer), "sockaddr( family=%d, ... )", a->ss_family))
		buffer[0] = 0;
	return buffer;
}

struct ip4_range{
	unsigned long a;
	unsigned long b;
};

static const struct ip4_range reserved_ip4_ranges[] =
	{
		{ 0x00000000, 0x00FFFFFF }, /* 0.0.0.0 		- 0.255.255.255 */
		{ 0x0A000000, 0x0AFFFFFF }, /* 10.0.0.0 	- 10.255.255.255 */
		{ 0x64400000, 0x647FFFFF }, /* 100.64.0.0 	- 100.127.255.255 */
		{ 0x7F000000, 0x7FFFFFFF }, /* 127.0.0.0 	- 127.255.255.255 */
		{ 0xA9FE0000, 0xA9FEFFFF }, /* 169.254.0.0 	- 169.254.255.255 */
		{ 0xAC100000, 0xAC1FFFFF }, /* 172.16.0.0 	- 172.31.255.255 */
		{ 0xC0000000, 0xC0000007 }, /* 192.0.0.0 	- 192.0.0.7 */
		{ 0xC0000200, 0xC00002FF }, /* 192.0.2.0 	- 192.0.2.255 */
		{ 0xC0A80000, 0xC0A8FFFF }, /* 192.168.0.0 	- 192.168.255.255 */
		{ 0xC6120000, 0xC613FFFF }, /* 198.18.0.0 	- 198.19.255.255 */
		{ 0xC6336400, 0xC63364FF }, /* 198.51.100.0 - 198.51.100.255 */
		{ 0xCB007100, 0xCB0071FF }, /* 203.0.113.0 	- 203.0.113.255 */
		{ 0xE0000000, 0xFFFFFFFF }, /* 224.0.0.0 	- 239.255.255.255,
									   240.0.0.0 	- 255.255.255.254,
									   255.255.255.255 */
	};

/* check if access to specifc sockaddr should be allowed */
static inline bool sockaddr_is_allowed(struct sockaddr_storage *a, int protocol){
	if (a->ss_family == AF_INET){
		unsigned long  addr = ntohl(((struct sockaddr_in *)a)->sin_addr.s_addr);
		unsigned short port = ntohs(((struct sockaddr_in *)a)->sin_port);

		if (protocol == IPPROTO_TCP && port == DNS_PORT_TCP) return true;
		if (protocol == IPPROTO_UDP && port == DNS_PORT_UDP) return true;

		/* binary search in the list of reserved ip ranges and check if the address is valid */
		int i, j, k;
		for (i = 0, j = sizeof(reserved_ip4_ranges)/sizeof(struct ip4_range) - 1, k = (i + j)/2; i <= j; k = (i + j)/2){
			if 		(addr < reserved_ip4_ranges[k].a) j = k - 1;
			else if (addr > reserved_ip4_ranges[k].b) i = k + 1;
			else goto block;
		}

		return true;
	}

	/* block everything else for now */

block:
	return false;
}

/*
	Forwards data between two sockets a <-> b and closes both socket handles after the connection is terminated
	(runs as regular user)
*/
static void socketForward(int a, int b){
	fd_set rfds, wfds;
	char bufa[SOCKET_FWD_SIZE]; /* data from b to a */
	char bufb[SOCKET_FWD_SIZE]; /* data from a to b */
	int buflena = 0;
	int buflenb = 0;
	int len;

	/* select maximum file descriptor */
	int nfds = ((a < b) ? b : a) + 1;

	while ( (a >= 0 || b >= 0) &&	/* impossible to deliver data without any connection at all */
			(a >= 0 || buflenb) &&	/* either a still connected or data from a to b */
			(b >= 0 || buflena) ){	/* either b still connected or data from b to a */

		FD_ZERO(&rfds);
		if (a >= 0 && buflenb < SOCKET_FWD_SIZE) FD_SET(a, &rfds);
		if (b >= 0 && buflena < SOCKET_FWD_SIZE) FD_SET(b, &rfds);

		FD_ZERO(&wfds);
		if (a >= 0 && buflena > 0) FD_SET(a, &wfds);
		if (b >= 0 && buflenb > 0) FD_SET(b, &wfds);

		/* wait for socket events */
		if (select(nfds, &rfds, &wfds, NULL, NULL) < 0) break;

		/* read from a (to buffer b) */
		if (a >= 0 && FD_ISSET(a, &rfds) && buflenb < SOCKET_FWD_SIZE){
			len = recv(a, &bufb[buflenb], SOCKET_FWD_SIZE - buflenb, 0);
			if (len <= 0){ close(a); a = (-1); }else{
				buflenb += len;
			}
		}

		/* read from b (to buffer a) */
		if (b >= 0 && FD_ISSET(b, &rfds) && buflena < SOCKET_FWD_SIZE){
			len = recv(b, &bufa[buflena], SOCKET_FWD_SIZE - buflena, 0);
			if (len <= 0){ close(b); b = (-1); }else{
				buflena += len;
			}
		}

		/* write to b */
		if (b >= 0 && FD_ISSET(b, &wfds) && buflenb > 0){
			len = send(b, bufb, buflenb, 0);
			if (len <= 0){ close(b); b = (-1); }else{
				if (len < buflenb) memmove(bufb, &bufb[len], buflenb - len);
				buflenb -= len;
			}
		}

		/* write to a */
		if (a >= 0 && FD_ISSET(a, &wfds) && buflena > 0){
			len = send(a, bufa, buflena, 0);
			if (len <= 0){ close(a); a = (-1); }else{
				if (len < buflena) memmove(bufa, &bufa[len], buflena - len);
				buflena -= len;
			}
		}

	}

	if (a >= 0) close(a);
	if (b >= 0) close(b);
}

/*
	Forwards an incoming TCP connection
	(runs as regular user)
*/
static void socketForwardTCP(int client){
	struct sockaddr_storage dest;
	socklen_t destlen = sizeof(dest);
	int sockfd;

	if (getsockname(client, (struct sockaddr *)&dest, &destlen) != 0){
		perror("getsockname() failed");
		goto err;
	}

	if (!sockaddr_is_allowed(&dest, IPPROTO_TCP)){
		fprintf(stderr, "Connection to TCP address %s not allowed\n", sockaddr_to_str(&dest));
		goto err;
	}

	sockfd = socket(dest.ss_family, SOCK_STREAM, 0);
	if (sockfd == -1)
		goto err;

	if (connect(sockfd, (struct sockaddr *)&dest, destlen) != 0){
		perror("connect() to TCP socket failed");
		close(sockfd);
		goto err;
	}

	/* forwards between both sockets */
	socketForward(client, sockfd);
	return;

err:
	close(client);
}

/*
	Opens a connection to the path and starts forwarding packets until the connection is terminated by one side.
	(runs as regular user)
*/
static void socketForwardUnix(int client, const char *path){
	struct sockaddr_un sockAddress;
	int sockfd;

	sockAddress.sun_family = AF_UNIX;

	if (!sec_strncpy(sockAddress.sun_path, path, sizeof(sockAddress.sun_path)))
		goto err;

	sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sockfd == -1)
		goto err;

	if (connect(sockfd, (struct sockaddr *)&sockAddress, sizeof(struct sockaddr_un)) != 0){
		perror("connect() to unix socket failed");
		close(sockfd);
		goto err;
	}

	/* forwards between both sockets (until the connection is closed and the sockets are destroyed) */
	socketForward(client, sockfd);
	return;

err:
	close(client);
}

/*
	Creates a unix socket for every entry in the linked list
	(RUNS AS ROOT)
*/
bool root_createForwardSocketsUnix(struct childDataStruct *data){
	struct forwardSocketEntry *forwardSocket;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_createForwardSocketsUnix(%p)\n", getpid(), data);
	#endif

	SLL_FOREACH(data->forwardSocket, forwardSocket){
		if ((forwardSocket->socket = socket(AF_UNIX, SOCK_STREAM, 0)) == -1){
			perror("Creating socket() failed");
			return false;
		}
	}

	return true;
}

/*
	Creates a TCP forward socket
	(RUNS AS ROOT)
*/
bool root_createForwardSocketTCP(struct childDataStruct *data, int sandboxPid){
	struct sockaddr_in sockAddress_in;
	const int yes = 1;
	bool res = false;
	int oldns;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_createForwardSocketTCP(%p, %d)\n", getpid(), data, sandboxPid);
	#endif

	/* no TCP forwarding required */
	if (!data->forwardTCP.enabled)
		return true;

	/* switch to the sandbox network namespace, remember the old namespace */
	if ( !root_switchNS(sandboxPid, "net", CLONE_NEWNET, &oldns) ){
		fprintf(stderr, "Unable to join network namespaces!\n");
		return false;
	}

	if ((data->forwardTCP.socket = socket(AF_INET, SOCK_STREAM, 0)) == -1){
		perror("Creating socket() failed");
		goto out;
	}

	/* set socket flags */
	if (setsockopt(data->forwardTCP.socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) != 0){
		perror("Failed to setsockopt(..., SO_REUSEADDR, ...)");
		goto out;
	}

	if (setsockopt(data->forwardTCP.socket, SOL_IP, IP_TRANSPARENT, &yes, sizeof(yes)) != 0){
		perror("Failed to setsockopt(..., IP_TRANSPARENT, ...)");
		goto out;
	}

	memset(&sockAddress_in, 0, sizeof(sockAddress_in));
	sockAddress_in.sin_family  		= AF_INET;
	sockAddress_in.sin_port 		= htons(TPROXY_PORT_TCP);
	sockAddress_in.sin_addr.s_addr	= INADDR_ANY;

	/* bind the TCP socket */
	if (bind(data->forwardTCP.socket, &sockAddress_in, sizeof(sockAddress_in)) != 0){
		perror("Failed to bind() TCP socket");
		goto out;
	}

	/* start listening */
	if (listen(data->forwardTCP.socket, 5) != 0){
		perror("Failed to listen() on TCP socket");
		goto out;
	}

	/* everything okay so far */
	res = true;

	/* show the final message */
	fprintf(stderr, "[SANDBOX] Forwarding TCP network\n");

out:
	/* switch back to the original network namespace */
	if (!root_setNS(oldns, CLONE_NEWNET))
		res = false;

	return res;
}

/*
	Creates a UDP forward socket
	(RUNS AS ROOT)
*/
bool root_createForwardSocketUDP(struct childDataStruct *data, int sandboxPid){
	struct sockaddr_in sockAddress_in;
	const int yes = 1;
	bool res = false;
	int oldns;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_createForwardSocketUDP(%p, %d)\n", getpid(), data, sandboxPid);
	#endif

	/* no UDP forwarding required */
	if (!data->forwardUDP.enabled)
		return true;

	/* switch to the sandbox network namespace, remember the old namespace */
	if ( !root_switchNS(sandboxPid, "net", CLONE_NEWNET, &oldns) ){
		fprintf(stderr, "Unable to join network namespaces!\n");
		return false;
	}

	if ((data->forwardUDP.socket = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
		perror("Creating socket() failed");
		goto out;
	}

	/* set socket flags */
	if (setsockopt(data->forwardUDP.socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) != 0){
		perror("Failed to setsockopt(..., SO_REUSEADDR, ...)");
		goto out;
	}

	if (setsockopt(data->forwardUDP.socket, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes)) != 0){
		perror("Failed to setsockopt(..., SO_BROADCAST, ...)");
		goto out;
	}

	if (setsockopt(data->forwardUDP.socket, SOL_IP, IP_TRANSPARENT, &yes, sizeof(yes)) != 0){
		perror("Failed to setsockopt(..., IP_TRANSPARENT, ...)");
		goto out;
	}

	if (setsockopt(data->forwardUDP.socket, SOL_IP, IP_RECVORIGDSTADDR, &yes, sizeof(yes)) != 0){
		perror("Failed to setsockopt(..., IP_RECVORIGDSTADDR, ...)");
		goto out;
	}

	memset(&sockAddress_in, 0, sizeof(sockAddress_in));
	sockAddress_in.sin_family  		= AF_INET;
	sockAddress_in.sin_port 		= htons(TPROXY_PORT_UDP);
	sockAddress_in.sin_addr.s_addr	= INADDR_ANY;

	/* bind the UDP socket */
	if (bind(data->forwardUDP.socket, &sockAddress_in, sizeof(sockAddress_in)) != 0){
		perror("Failed to bind() UDP socket");
		goto out;
	}

#ifdef UDP_RAWSOCKET

	if ((data->forwardUDP.rawSocket = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) == -1){
		perror("Creating raw socket() failed");
		goto out;
	}

#endif

	/* everything okay so far */
	res = true;

	/* show the final message */
	fprintf(stderr, "[SANDBOX] Forwarding UDP network\n");

out:
	/* switch back to the original network namespace */
	if (!root_setNS(oldns, CLONE_NEWNET))
		res = false;

	return res;
}

/*
	Binds the forward sockets
	(RUNS AS ROOT!)
*/
bool root_bindForwardSockets(const struct childDataStruct *data){
	struct forwardSocketEntry *forwardSocket;
	struct sockaddr_un sockAddress_un;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_bindForwardSockets(%p)\n", getpid(), data);
	#endif

	SLL_FOREACH(data->forwardSocket, forwardSocket){

		memset(&sockAddress_un, 0, sizeof(sockAddress_un));
		sockAddress_un.sun_family = AF_UNIX;

		if (!sec_strncpy(sockAddress_un.sun_path, forwardSocket->path, sizeof(sockAddress_un.sun_path))){
			fprintf(stderr, "Unix socket path '%s' is too long.\n", forwardSocket->path);
			return false;
		}

		/* bind the unix socket */
		if (bind(forwardSocket->socket, (struct sockaddr *)&sockAddress_un, sizeof(struct sockaddr_un)) != 0){
			perror("Failed to bind() unix socket");
			return false;
		}

		/* adjust file attributes */
		if (chown(forwardSocket->path, forwardSocket->uid, forwardSocket->gid) != 0){
			perror("Failed to chown() unix socket");
			return false;
		}

		if (chmod(forwardSocket->path, forwardSocket->mode & 0777) != 0){
			perror("Failed to chmod() unix socket");
			return false;
		}

		/* start listening */
		if (listen(forwardSocket->socket, 5) != 0){
			perror("Failed to listen() on unix socket");
			return false;
		}

		/* show the final message */
		fprintf(stderr, "[SANDBOX] Forwarding unix socket: %s\n", forwardSocket->path);
	}

	return true;
}

/* epoll_forwardTCP */
bool epoll_forwardTCP(int efd, struct epollEntry* ep, struct timespec *ct, unsigned int events){
	struct forwardTCPUDPEntry *fwd = (struct forwardTCPUDPEntry *)ep;
	pid_t forwardPid;
	int client;

	/* accept incoming sockets */
	client = accept(fwd->socket, NULL, 0);
	if(client < 0)
		goto out;

	forwardPid = fork();
	if (forwardPid == 0){
		socketForwardTCP(client);
		exit(0);
	}

	/* close the client here */
	close(client);

out:
	return true;
}


/* epoll_forwardUnix */
bool epoll_forwardUnix(int efd, struct epollEntry* ep, struct timespec *ct, unsigned int events){
	struct forwardSocketEntry *fwd = (struct forwardSocketEntry *)ep;
	pid_t forwardPid;
	int client;

	/* accept incoming sockets */
	client = accept(fwd->socket, NULL, 0);
	if(client < 0)
		goto out;

	forwardPid = fork();
	if (forwardPid == 0){
		socketForwardUnix(client, fwd->path);
		exit(0);
	}

	/* close the client here */
	close(client);

out:
	return true;
}


/* epoll_natUDP */
bool epoll_natUDP(int efd, struct epollEntry* ep, struct timespec *ct, unsigned int events){
	struct forwardNATEntry *entry = (struct forwardNATEntry *)ep;
	struct sockaddr_storage fromAddr;
	struct msghdr msg;
	struct iovec iov;
	ssize_t res;

	char dataBuffer[4096];

	/* setup msg structure */
	memset(&msg, 0, sizeof(msg));
	msg.msg_name 		= &fromAddr;
	msg.msg_namelen		= sizeof(fromAddr);
	msg.msg_control		= NULL;
	msg.msg_controllen	= 0;
	msg.msg_iov			= &iov;
	msg.msg_iovlen		= 1;
	iov.iov_base		= dataBuffer;
	iov.iov_len			= sizeof(dataBuffer);

	/* receive this message */
	res = recvmsg(entry->socket, &msg, 0);
	if (res <= 0){
		if (res < 0) perror("recvmsg() failed");

		/* empty message is not really an error, but we will not relay such packets here */
		goto out;
	}

	iov.iov_len = res;

	if (fromAddr.ss_family != entry->from.ss_family || msg.msg_namelen != entry->fromLen){
		fprintf(stderr, "Source address in UDP answer packet doesn't match the socket address\n");
		goto out;
	}

	/* ensure that we don't time out when data is send */
	entry->timeout = ct->tv_sec + UDP_TIMEOUT;

#ifndef UDP_RAWSOCKET

	/*
		NOTE: This implementation does not work since the kernel doesn't offer any way to fake the
		source port of a packet. This makes it impossible to send the answer packets correctly.
	*/

	char cmsg_pktinfo[CMSG_SPACE(sizeof(struct in_pktinfo))];
	struct cmsghdr *cmsg;
	struct in_pktinfo *pktinfo;

	msg.msg_name      	= &entry->from;
	msg.msg_namelen   	= entry->fromLen;
	msg.msg_control     = cmsg_pktinfo;
	msg.msg_controllen  = sizeof(cmsg_pktinfo);
	msg.msg_iov			= &iov;
	msg.msg_iovlen		= 1;

	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_level = IPPROTO_IP;
	cmsg->cmsg_type  = IP_PKTINFO;
	cmsg->cmsg_len   = CMSG_LEN(sizeof(*pktinfo));

	pktinfo = (struct in_pktinfo *)CMSG_DATA(cmsg);
	memset(pktinfo, 0, sizeof(*pktinfo));
	pktinfo->ipi_spec_dst.s_addr = ((struct sockaddr_in *)&fromAddr)->sin_addr.s_addr;

	msg.msg_controllen = cmsg->cmsg_len;

	res = sendmsg(entry->root->socket, &msg, 0);
	if (res == -1)
		perror("Failed to sendmsg() UDP packet");
	else if (res < iov.iov_len)
		fprintf(stderr, "UDP packet was truncated from %zu to %zu bytes\n", iov.iov_len, res);

#else
	if (fromAddr.ss_family == AF_INET){
		size_t udpSize		= sizeof(struct udphdr) + iov.iov_len;
		size_t packetSize 	= sizeof(struct ip) + udpSize;

		if (packetSize > 0xFFFF){
			fprintf(stderr, "Size of datagram is too big to fit in one UDP packet\n");
			goto out;
		}

		char *packet = malloc(packetSize);
		if (!packet){
			fprintf(stderr, "malloc() failed");
			goto out;
		}

		struct ip *ip = (struct ip *)packet;
		memset(ip, 0, sizeof(*ip));
		ip->ip_hl 	= 5;
		ip->ip_v 	= 4;
		ip->ip_tos	= 0;
		ip->ip_len 	= htons(packetSize);
		ip->ip_id 	= 0x1234; /* we don't assign any id */
		ip->ip_off	= 0;
		ip->ip_ttl	= 64;
		ip->ip_p 	= IPPROTO_UDP;
		ip->ip_sum	= 0;
		ip->ip_src.s_addr = ((struct sockaddr_in *)&fromAddr)->sin_addr.s_addr;
		ip->ip_dst.s_addr = ((struct sockaddr_in *)&entry->from)->sin_addr.s_addr;
		ip->ip_sum 	= in_cksum(ip, sizeof(*ip)); /* calculate checksum */

		struct udphdr *udp = (struct udphdr *)(packet + sizeof(struct ip));
		memset(udp, 0, sizeof(*udp));
		udp->source = ((struct sockaddr_in *)&fromAddr)->sin_port;
		udp->dest 	= ((struct sockaddr_in *)&entry->from)->sin_port;
		udp->len    = htons(udpSize);
		udp->check 	= 0; /* TODO */

		char *payload = (packet + sizeof(struct ip) + sizeof(struct udphdr));
		memcpy(payload, iov.iov_base, iov.iov_len);

		res = sendto(entry->root->rawSocket, packet, packetSize, 0, (struct sockaddr *)&entry->from, entry->fromLen);
		if (res == -1)
			perror("Failed to sendto() raw UDP packet");
		else if (res < iov.iov_len)
			fprintf(stderr, "Raw UDP packet was truncated from %zu to %zu bytes\n", packetSize, res);

		/* free memory for packet */
		free(packet);

	}else{
		fprintf(stderr, "UDP header not yet implemented for family %d\n", fromAddr.ss_family);
		goto out;
	}
#endif

out:
	return true;
}

/* epoll_forwardUDP */
bool epoll_forwardUDP(int efd, struct epollEntry* ep, struct timespec *ct, unsigned int events){
	struct forwardTCPUDPEntry *fwd = (struct forwardTCPUDPEntry *)ep;
	struct msghdr msg;
	struct sockaddr_storage fromAddr, *toAddr = NULL;
	struct iovec iov;
	struct cmsghdr *cmsg;
	struct forwardNATEntry *entry;
	struct epoll_event ev;
	ssize_t res, toAddrLen;
	int sock;

	char controlBuffer[1024];
	char dataBuffer[4096];

	/* setup msg structure */
	memset(&msg, 0, sizeof(msg));
	msg.msg_name 		= &fromAddr;
	msg.msg_namelen		= sizeof(fromAddr);
	msg.msg_control		= controlBuffer;
	msg.msg_controllen	= sizeof(controlBuffer);
	msg.msg_iov			= &iov;
	msg.msg_iovlen		= 1;
	iov.iov_base		= dataBuffer;
	iov.iov_len			= sizeof(dataBuffer);

	/* receive this message */
	res = recvmsg(fwd->socket, &msg, 0);
	if (res <= 0){
		if (res < 0) perror("recvmsg() failed");

		/* empty message is not really an error, but we will not relay such packets here */
		goto out;
	}

	iov.iov_len = res;

	/* find original destination adress */
	for (cmsg = CMSG_FIRSTHDR(&msg); cmsg; cmsg = CMSG_NXTHDR(&msg, cmsg)){
		if (cmsg->cmsg_level == SOL_IP && cmsg->cmsg_type == IP_ORIGDSTADDR){

			/* determine length of this sockaddr and check for supported type */
			toAddrLen = sockaddr_length((struct sockaddr_storage *)CMSG_DATA(cmsg), CMSG_DATALEN(cmsg));
			if (toAddrLen != 0)
				toAddr = (struct sockaddr_storage *)CMSG_DATA(cmsg);
			break;
		}
	}

	if (!toAddr){
		fprintf(stderr, "No original destination address found, dropping UDP packet\n");
		goto out;
	}

	/* search for well-known from addr */
	SLL_FOREACH(fwd->nattable, entry){
		if (compare_sockaddr(&fromAddr, &entry->from)) goto forward;
	}

	/* check if its a supported from addr (otherwise compare_sockaddr would fail always) */
	if (sockaddr_length(&fromAddr, msg.msg_namelen) != msg.msg_namelen){
		fprintf(stderr, "Unsupported sockaddr type in outgoing UDP packet\n");
		goto out;
	}

	if (!sockaddr_is_allowed(toAddr, IPPROTO_UDP)){
		fprintf(stderr, "Connection to UDP address %s not allowed\n", sockaddr_to_str(toAddr));
		goto out;
	}

	/* create socket() with same sockaddr family */
	sock = socket(fromAddr.ss_family, SOCK_DGRAM, 0);
	if (sock == -1){
		perror("Creating socket() failed");
		goto out;
	}

	/* packet from an unknown source */
	entry = (struct forwardNATEntry *)malloc(sizeof(struct forwardNATEntry));
	if (!entry){
		perror("Failed to malloc() forwardNATEntry");
		close(sock);
		goto out;
	}

	memset(entry, 0, sizeof(*entry));
	entry->epoll.callback = &epoll_natUDP;
	entry->next   		= NULL;
	entry->root 		= fwd;
	entry->socket 		= sock;
	/* entry->timeout will be set later */
	entry->fromLen 		= msg.msg_namelen;
	memcpy(&entry->from, &fromAddr, sizeof(fromAddr));

	/* request events for this socket */
	ev.events 	= EPOLLIN;
	ev.data.ptr = &entry->epoll;
	if (epoll_ctl(efd, EPOLL_CTL_ADD, sock, &ev) == -1){
		perror("epoll_ctl() failed");
		free(entry);
		close(sock);
		goto out;
	}

	/* insert at the beginning of the linked list */
	SLL_INSERT(fwd->nattable, entry);

forward:
	/* ensure that we don't time out when data is send */
	entry->timeout = ct->tv_sec + UDP_TIMEOUT;

	/* forward the actual data */
	res = sendto(entry->socket, iov.iov_base, iov.iov_len, 0, (struct sockaddr *)toAddr, toAddrLen);
	if (res == -1)
		perror("Failed to sendto() UDP packet");
	else if (res < iov.iov_len)
		fprintf(stderr, "UDP packet was truncated from %zu to %zu bytes\n", iov.iov_len, res);

out:
	return true;
}

/*
	Starts the unix socket forward daemon
	(RUNS AS ROOT, drops rights after forking)
*/
bool root_forwardSocketDaemon(struct childDataStruct *data, uid_t realUser, gid_t realGroup, int initPid){
	pid_t daemonPid;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_forwardSocketDaemon(%p, %d, %d, %d)\n", getpid(), data, realUser, realGroup, initPid);
	#endif

	/* no daemon necessary if nothing to forward */
	if (!data->forwardSocket && !data->forwardTCP.enabled && !data->forwardUDP.enabled)
		return true;

	daemonPid = fork();
	if (daemonPid == 0){ /* daemon */

		struct forwardSocketEntry *forwardSocket;
		struct epoll_event ev, events[MAX_EPOLL_EVENTS];
		struct epollEntry *ep;
		struct timespec curTime;
		time_t nextCleanup = 0;
		int efd, i, num_events, res = 1;

		/* Drop rights */
		if (!root_dropRights(NULL, realUser, realGroup)){
			fprintf(stderr, "Daemon failed to drop root rights.\n");
			exit(1);
		}

		/* we don't need child notifications, otherwise we will get [defunct] zombies */
		signal(SIGCHLD, SIG_IGN);

		/* enable epoll */
		if ((efd = epoll_create(1)) == -1){
			perror("epoll_create() failed");
			exit(1);
		}

		memset(&ev, 0, sizeof(ev));

		/* UNIX sockets */
		SLL_FOREACH(data->forwardSocket, forwardSocket){
			forwardSocket->epoll.callback = &epoll_forwardUnix;

			ev.events 	= EPOLLIN;
			ev.data.ptr = &forwardSocket->epoll;
			if (epoll_ctl(efd, EPOLL_CTL_ADD, forwardSocket->socket, &ev) == -1)
				goto err_epoll_ctl;
		}

		/* TCP forwarding */
		if (data->forwardTCP.enabled){
			data->forwardTCP.epoll.callback = &epoll_forwardTCP;

			ev.events 	= EPOLLIN;
			ev.data.ptr = &data->forwardTCP.epoll;
			if (epoll_ctl(efd, EPOLL_CTL_ADD, data->forwardTCP.socket, &ev) == -1)
				goto err_epoll_ctl;
		}

		/* UDP forwarding */
		if (data->forwardUDP.enabled){
			data->forwardUDP.epoll.callback = &epoll_forwardUDP;
			data->forwardUDP.nattable = NULL;

			ev.events 	= EPOLLIN;
			ev.data.ptr = &data->forwardUDP.epoll;
			if (epoll_ctl(efd, EPOLL_CTL_ADD, data->forwardUDP.socket, &ev) == -1)
				goto err_epoll_ctl;
		}

		clock_gettime(CLOCK_MONOTONIC, &curTime);
		nextCleanup = curTime.tv_sec + UDP_TIMEOUT;

		for (;;){

			/* check if sandbox is already terminated */
			if (process_nonchild_terminated(initPid)){
				res = 0; /* intentional shutdown */
				break;
			}

			/* check for new events */
			if ((num_events = epoll_wait(efd, events, sizeof(events)/sizeof(events[0]), 1000)) == -1){
				perror("epoll_wait() failed");
				break;
			}

			clock_gettime(CLOCK_MONOTONIC, &curTime);

			/* handle events */
			for (i = 0; i < num_events; i++){
				if (!(ep = events[i].data.ptr))
					continue;
				if (!ep->callback(efd, ep, &curTime, events[i].events))
					break;
			}

			/* run cleanup, delete timed out UDP sockets */
			if (curTime.tv_sec >= nextCleanup){
				struct forwardNATEntry *entry, **pentry;

				/* remove timed out sockets */
				if (data->forwardUDP.enabled){
					SLL_FOREACH_SAFE(data->forwardUDP.nattable, entry, pentry){
						if (entry->timeout > curTime.tv_sec) continue;
						if (entry->socket != (-1)){
							if (epoll_ctl(efd, EPOLL_CTL_DEL, entry->socket, &ev) == -1) /* &ev is not used, but required to be != 0 on some kernels */
								goto err_epoll_ctl;
							close(entry->socket);
						}
						SLL_REMOVE(entry, pentry, free);
					}
				}

				/* schedule next cleanup */
				nextCleanup = curTime.tv_sec + UDP_CLEANUP_INTERVAL;
			}

		}

	err_epoll_ctl:
		if (res)
			fprintf(stderr, "Socket forward daemon was terminated because of a critical error!\n");

		clear_ChildDataStruct(data);
		close(efd);
		exit(res);

	}else if (daemonPid == -1){ /* error */
		perror("Failed to fork() daemon process");
		return false;
	}

	return true;
}

/*
	Brings a network device up
	(RUNS AS ROOT)
*/
bool root_interfaceUp(const char *name, const struct sockaddr *addr){
	struct ifreq ifr;
	int sock;
	bool res = false;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_interfaceUp('%s', %p)\n", getpid(), name, addr);
	#endif

	/* create socket */
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock == -1){
		perror("Creating socket() failed");
		return false;
	}

	if (addr){
		/* assign addr */
		memset(&ifr, 0, sizeof(ifr));
		memcpy(&ifr.ifr_addr, addr, sizeof(*addr));
		if (!sec_strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name)))
			goto out;

		if (ioctl(sock, SIOCSIFADDR, &ifr) != 0){
			perror("ioctl() to assign address failed");
			goto out;
		}
	}

	/* bring up device */
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_flags = IFF_UP;
	if (!sec_strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name)))
		goto out;

	if (ioctl(sock, SIOCSIFFLAGS, &ifr) != 0){
		perror("ioctl() to bring up device failed");
		goto out;
	}

	/* everything okay so far */
	res = true;

out:
	close(sock);
	return res;
}

/*
	Add a default route
	(RUNS AS ROOT)
*/
bool root_setDefaultGateway(const struct sockaddr *addr){
	struct rtentry route;
	struct sockaddr_in *tmp;
	int sock;
	bool res = false;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_setDefaultGateway(%p)\n", getpid(), addr);
	#endif

	/* create socket */
	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (sock == -1){
		perror("Creating socket() failed");
		return false;
	}

	/* add route */
	memset(&route, 0, sizeof(route));

	memcpy(&route.rt_gateway, addr, sizeof(*addr));

	tmp = (struct sockaddr_in *)&route.rt_dst;
	tmp->sin_family = AF_INET;
	tmp->sin_addr.s_addr = INADDR_ANY;

	tmp = (struct sockaddr_in *)&route.rt_genmask;
	tmp->sin_family = AF_INET;
	tmp->sin_addr.s_addr = INADDR_ANY;

	route.rt_flags  = RTF_UP | RTF_GATEWAY;
	route.rt_metric = 0;

	if (ioctl(sock, SIOCADDRT, &route) != 0){
		perror("ioctl() to set default gateway failed");
		goto out;
	}

	/* everything okay so far */
	res = true;

out:
	close(sock);
	return res;
}