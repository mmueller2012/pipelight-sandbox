#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <stdlib.h>
#include <mntent.h>
#include <stdio.h>
#include <errno.h>

#include "sandbox.h"

/*
	Checks if a directory is a mountpoint, returns MOUNTPOINT_{ERROR,EXISTS,NONE}.
	NOTE: Checking on / is not allowed and will return MOUNTPOINT_ERROR
	(RUNS AS ROOT!)
*/
int root_isMountpoint(const char *path){
	char pathBuffer[PATH_MAX];
	struct stat fileInfo, fileInfo2;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_isMountpoint('%s')\n", getpid(), path);
	#endif

	if (!sec_snprintf(pathBuffer, PATH_MAX, "%s/..", path))
		return MOUNTPOINT_ERROR;

	if (stat(path, &fileInfo) != 0)
		return MOUNTPOINT_ERROR;

	if (stat(pathBuffer, &fileInfo2) != 0)
		return MOUNTPOINT_ERROR;

	return (fileInfo.st_dev != fileInfo2.st_dev) ? MOUNTPOINT_EXISTS : MOUNTPOINT_NONE;
}

/*
	base is the root path of the chroot environment
	path must exist in the real filesystem
*/
bool root_mkdirRecursive(const char *base, const char *path){
	char *canonPath = NULL;
	char *tmpPtr;
	char pathBuffer[PATH_MAX];
	bool res = false;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_mkdirRecursive('%s', '%s')\n", getpid(), base, path);
	#endif

	/* get the full path */
	canonPath = canonicalize_file_name(path);
	if (!canonPath){
		perror("Calling canonicalize_file_name() on directory failed");
		return false;
	}

	/* NOTE: we have to free the canonPath ptr */

	if (canonPath[0] != '/'){
		fprintf(stderr, "Canonicalized path is not valid.\n");
		goto out;
	}

	for (tmpPtr = canonPath + 1; *tmpPtr ; tmpPtr++){
		if (*tmpPtr != '/') continue;

		*tmpPtr = 0;

		if (!sec_snprintf(pathBuffer, PATH_MAX, "%s%s", base, canonPath)){
			fprintf(stderr, "Failed to create parent directories.\n");
			goto out;
		}

		if (mkdir(pathBuffer, 0775) != 0 && errno != EEXIST){
			perror("Failed to mkdir()");
			goto out;
		}

		*tmpPtr = '/';
	}

	/* everything okay */
	res = true;

out:
	free(canonPath);
	return res;
}

/*
	Duplicate mounts from original mount table
	(RUNS AS ROOT)
*/
bool root_duplicateMnt(const struct childDataStruct *data){
	FILE *mntFile;
	struct mntent *mntEnt;
	struct stringListEntry *mntListHead = NULL, *mntListTail = NULL, *mntListEntry;
	char pathBuffer[PATH_MAX];
	bool res = false;

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_duplicateMnt(%p)\n", getpid(), data);
	#endif

	/* read the mounts first and merge them to ensure that we don't mess up something */
	mntFile = setmntent("/proc/mounts", "r");
	if (!mntFile) {
		perror("Unable to setmntent() enumerate mounts");
		return false;
	}

	while ((mntEnt = getmntent(mntFile)) != NULL){

		/*
			check if this is a file system mount point
			or something else
		*/
		if (mntEnt->mnt_dir[0] != '/')
			continue;

		/* delete mounts for subdirectories from the list */
		SLL_FOREACH(mntListHead, mntListEntry){
			if (!mntListEntry->str) continue;
			if (!dir_starts_with_str(mntListEntry->str, mntEnt->mnt_dir)) continue;
			free(mntListEntry->str);
			mntListEntry->str = NULL;
		}

		mntListEntry = alloc_StringListEntry(mntEnt->mnt_dir);
		if (!mntListEntry) {
			fprintf(stderr, "Failed to add entry to mount table.\n");
			goto out;
		}

		/* append at the end of the list */
		SLL_APPEND(mntListHead, mntListTail, mntListEntry);
	}

	/* NOTE: all the following mounts don't affect the space outside! */
	SLL_FOREACH(mntListHead, mntListEntry){
		if(!mntListEntry->str) continue;

		/* skip over mounts in CHROOTDIR */
		if (dir_starts_with(mntListEntry->str, CHROOTDIR))
			continue;

		/* do not mount these directories */
		if (	dir_starts_with(mntListEntry->str, "/dev") 	|| \
				dir_starts_with(mntListEntry->str, "/run") 	|| \
				dir_starts_with(mntListEntry->str, "/sys") 	|| \
				dir_starts_with(mntListEntry->str, "/proc")	|| \
				dir_starts_with(mntListEntry->str, "/tmp")	)
			continue;

		if (!sec_snprintf(pathBuffer, PATH_MAX, "%s%s", CHROOTDIR, mntListEntry->str)){
			fprintf(stderr, "Your chroot directory " CHROOTDIR " is too long.\n");
			goto out;
		}

		/* already mounted or unable to check -> error */
		if (root_isMountpoint(pathBuffer) != MOUNTPOINT_NONE){
			fprintf(stderr, "Unexpected mountpoint.\n");
			goto out;
		}

		/* it's not possible to specify MS_RDONLY directly */
		if (mount(mntListEntry->str, pathBuffer, NULL, MS_BIND | MS_NOSUID, NULL) != 0){
			perror("Failed to mount() bind directory");
			goto out;
		}

		if (mount(mntListEntry->str, pathBuffer, NULL, MS_BIND | MS_REMOUNT | MS_RDONLY | MS_NOSUID, NULL) != 0){
			perror("Failed to mount() rebind with readonly+nosuid rights");
			goto out;
		}

		/* if its not mounted correctly (or we cannot verify it) we try to unmount it again */
		if (root_isMountpoint(pathBuffer) != MOUNTPOINT_EXISTS && umount(pathBuffer) != 0){
			perror("Unable to umount() directory");
			goto out;
		}

	}

	/* ready */
	res = true;

out:
	free_StringList(&mntListHead);
	endmntent(mntFile);
	return res;
}

/*
	Setup mount namespace for chroot
	(RUNS AS ROOT!)
*/
bool root_setupChrootMnt(const struct childDataStruct *data){
	struct writeDirEntry *writeDir;
	struct forwardSocketEntry *forwardSocket;
	struct stat resolvInfo;
	char pathBuffer[PATH_MAX];

	#ifdef SANDBOXDEBUG
		fprintf(stderr, "[SANDBOX:%d] root_setupChrootMnt(%p)\n", getpid(), data);
	#endif

	if (geteuid() != 0){
		fprintf(stderr, "Unable to setup chroot mnt without root rights.\n");
		return false;
	}

	if (mkdir(CHROOTDIR, 700) != 0 && errno != EEXIST){
		perror("Failed to mkdir() chroot directory " CHROOTDIR);
		return false;
	}

	/* ensure that we don't have any mountpoints yet */
	if (root_isMountpoint(CHROOTDIR) != MOUNTPOINT_NONE){
		fprintf(stderr, "There is something wrong with the directory " CHROOTDIR ".\n");
		return false;
	}

	/* duplicate the mounts */
	if (!root_duplicateMnt(data)){
		fprintf(stderr, "Unable to duplicate mountpoints.\n");
		return false;
	}

	/* writeable paths (if given) */
	SLL_FOREACH(data->writeDir, writeDir){

		/* do not mount these directories as writedir */
		if (	dir_starts_with(writeDir->path, "/dev") 	|| \
				dir_starts_with(writeDir->path, "/run") 	|| \
				dir_starts_with(writeDir->path, "/sys") 	|| \
				dir_starts_with(writeDir->path, "/proc")	|| \
				dir_starts_with(writeDir->path, "/root")	)
			continue;

		if (!sec_snprintf(pathBuffer, PATH_MAX, "%s%s", CHROOTDIR, writeDir->path)){
			fprintf(stderr, "Your chroot directory " CHROOTDIR " is too long.\n");
			return false;
		}

		if (mount(writeDir->path, pathBuffer, NULL, MS_BIND | MS_NOSUID | MS_NODEV, NULL) != 0){
			perror("Failed to mount() bind write directory");
			return false;
		}

		/* show the final message */
		fprintf(stderr, "[SANDBOX] Mounted with write access: %s\n", writeDir->path);
	}

	/* mount proc, so that we will only see processes inside */
	if (mount("none", CHROOTDIR "/proc", "proc", MS_NOSUID | MS_NOEXEC | MS_NODEV, NULL) != 0){
		perror("Failed to mount() /proc");
		return false;
	}

	/* mount /dev as read-write */
	if (mount("/dev", CHROOTDIR "/dev", NULL, MS_BIND | MS_NOSUID | MS_NOEXEC | MS_REC, NULL) != 0){
		perror("Failed to mount() bind /dev");
		return false;
	}

	/* we need a /tmp directory, so we'll mount a tmpfs inside the sandbox */
	if (mount("none", CHROOTDIR "/tmp", "tmpfs", MS_NOSUID | MS_NODEV | MS_NOEXEC, NULL) != 0){
		perror("Failed to mount() /tmp");
		return false;
	}

	/* check whether "/run" is a mountpoint and mount a tmpfs over it*/
	if (root_isMountpoint("/run") == MOUNTPOINT_EXISTS){
		if (mount("none", CHROOTDIR "/run", "tmpfs", MS_NOSUID | MS_NODEV | MS_NOEXEC, NULL) != 0){
			perror("Failed to mount() /run");
			return false;
		}

		/* Some Linux distributions like Ubuntu store the real resolv.conf in /run/resolvconf */
		if (stat("/run/resolvconf/", &resolvInfo) == 0 && S_ISDIR(resolvInfo.st_mode)){
			if (mkdir(CHROOTDIR "/run/resolvconf", 0755) != 0 && errno != EEXIST){
				perror("Failed to create /run/resolvconf in chroot");
				return false;
			}

			/* bind resolv config */
			if (mount("/run/resolvconf/", CHROOTDIR "/run/resolvconf/", NULL, MS_BIND | MS_NOSUID | MS_NODEV, NULL) != 0){
				perror("Failed to mount() bind /run/resolvconf");
				return false;
			}

			/* remount it as read only, we do not want our chroot to mess around with it */
			if (mount("/run/resolvconf/", CHROOTDIR "/run/resolvconf/", NULL, MS_BIND | MS_REMOUNT | MS_RDONLY | MS_NOSUID | MS_NODEV, NULL) != 0){
				perror("Failed to mount() bind /run/resolvconf as readonly");
				return false;
			}
		}
	}

	/* create directories for unix sockets */
	SLL_FOREACH(data->forwardSocket, forwardSocket){
		if (!root_mkdirRecursive(CHROOTDIR, forwardSocket->path)){
			fprintf(stderr,"Failed to create parent directory for unix socket '%s'.\n", forwardSocket->path);
			return false;
		}
	}

	return true;
}

