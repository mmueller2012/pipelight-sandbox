CC 			:= gcc
CFLAGS 		:= -Werror -Wall $(CFLAGS)
LDFLAGS 	:= -lrt -lip4tc
OBJECTS		:= src/sandbox.o src/forward.o src/misc.o src/mount.o src/parseenv.o src/rights.o src/shm.o src/struct.o src/iptables.o

.PHONY: all
all: src/pipelight-sandbox

# pipelight-sandbox
src/pipelight-sandbox: $(OBJECTS)
	$(CC) $(CFLAGS) -z defs $(OBJECTS) $(LDFLAGS) -o src/pipelight-sandbox

src/sandbox.o: src/sandbox.c src/sandbox.h
	$(CC) $(CFLAGS) -c src/sandbox.c -o src/sandbox.o

src/forward.o: src/forward.c src/sandbox.h
	$(CC) $(CFLAGS) -c src/forward.c -o src/forward.o

src/misc.o: src/misc.c src/sandbox.h
	$(CC) $(CFLAGS) -c src/misc.c -o src/misc.o

src/mount.o: src/mount.c src/sandbox.h
	$(CC) $(CFLAGS) -c src/mount.c -o src/mount.o

src/parseenv.o: src/parseenv.c src/sandbox.h
	$(CC) $(CFLAGS) -c src/parseenv.c -o src/parseenv.o

src/rights.o: src/rights.c src/sandbox.h
	$(CC) $(CFLAGS) -c src/rights.c -o src/rights.o

src/shm.o: src/shm.c src/sandbox.h
	$(CC) $(CFLAGS) -c src/shm.c -o src/shm.o

src/struct.o: src/struct.c src/sandbox.h
	$(CC) $(CFLAGS) -c src/struct.c -o src/struct.o

src/iptables.o: src/iptables.c src/sandbox.h
	$(CC) $(CFLAGS) -c src/iptables.c -o src/iptables.o

# other
.PHONY: clean
clean:
	rm -f src/pipelight-sandbox src/*.o src/*.so

install: src/pipelight-sandbox
	test -d $(DESTDIR)/usr/bin/ || mkdir -p $(DESTDIR)/usr/bin/
	install -m 4755 src/pipelight-sandbox $(DESTDIR)/usr/bin/pipelight-sandbox
	install -m 0755 scripts/wine-sbox $(DESTDIR)/usr/bin/wine-sbox

uninstall:
	rm -f $(DESTDIR)/usr/bin/pipelight-sandbox
	rm -f $(DESTDIR)/usr/bin/wine-sbox
